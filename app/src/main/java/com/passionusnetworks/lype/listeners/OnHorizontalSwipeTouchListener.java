package com.passionusnetworks.lype.listeners;

import android.graphics.PointF;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Raphael on 2016-04-04.
 */
public abstract class OnHorizontalSwipeTouchListener implements View.OnTouchListener {
    private final String TAG = getClass().getSimpleName();

    public static float MINUMUM_SWIPE_END_SCREEN_PERCENTAGE_DISTANCE = 0.25f;
    protected static float MINIMUM_SWIPE_START_DISTANCE = 50;
    protected static float MINIMUM_SWIPE_END_DISTANCE = 250;

    PointF lastMotionEventPoint = new PointF();
    float totalDistanceX;
    float totalDistanceY;
    boolean canceled;
    boolean swiping;

    public static float getMinimumSwipeStartDistance() {
        return MINIMUM_SWIPE_START_DISTANCE;
    }

    public static void setMinimumSwipeStartDistance(float minimumSwipeStartDistance) {
        MINIMUM_SWIPE_START_DISTANCE = minimumSwipeStartDistance;
    }

    public static float getMinimumSwipeEndDistance() {
        return MINIMUM_SWIPE_END_DISTANCE;
    }

    public static void setMinimumSwipeEndDistance(float minimumSwipeEndDistance) {
        MINIMUM_SWIPE_END_DISTANCE = minimumSwipeEndDistance;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                return onDown(event);
            case MotionEvent.ACTION_MOVE:
                float distanceX = event.getRawX() - lastMotionEventPoint.x;
                float distanceY = event.getRawY() - lastMotionEventPoint.y;
                lastMotionEventPoint.set(event.getRawX(), event.getRawY());
                return onScroll(distanceX, distanceY);
            case MotionEvent.ACTION_UP:
                return onUp();
        }
        return false;
    }

    public boolean onDown(MotionEvent e) {
        lastMotionEventPoint.set(e.getRawX(), e.getRawY());
        totalDistanceX = totalDistanceY = 0;
        canceled = swiping = false;
        return false;
    }

    public boolean onScroll(float distanceX, float distanceY) {
        if(canceled)
            return false;
        totalDistanceX += distanceX;
        totalDistanceY += distanceY;
        float absTotalDistanceX = Math.abs(totalDistanceX);
        float absTotalDistanceY = Math.abs(totalDistanceY);

        if(!swiping && absTotalDistanceY > MINIMUM_SWIPE_START_DISTANCE) {
            canceled = true;
            return false;
        }

        if(absTotalDistanceX >= MINIMUM_SWIPE_START_DISTANCE && !swiping) {
            swiping = true;
            totalDistanceX = 0;
        }

        if(swiping) {
            swiping(totalDistanceX);
        }

        return swiping;
    }

    public boolean onUp() {
        if(swiping) {
            if(Math.abs(totalDistanceX) > MINIMUM_SWIPE_END_DISTANCE)
                onSwipe(totalDistanceX < 0 ? Gravity.LEFT : Gravity.RIGHT);
            else
                onSwipeCanceled();
        }
        return false;
    }

    protected abstract void swiping(float distance);

    protected abstract void onSwipe(int direction);

    protected abstract void onSwipeCanceled();
}

package com.passionusnetworks.lype.listeners;

/**
 * Created by Raphael on 2017-03-20.
 */

public interface FavoritesListListener {
    void onLastItemReached();
}

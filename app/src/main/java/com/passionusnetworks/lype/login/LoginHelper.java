package com.passionusnetworks.lype.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.persistence.PropertiesManager;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONException;
import org.json.JSONObject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2017-05-22.
 */
public class LoginHelper implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "LoginHelper";
    private static final int RC_SIGN_IN = 0;

    private LoginResultListener loginResultListener;
    private CallbackManager facebookCallbackManager;
    private GoogleApiClient googleApiClient;
    private FragmentActivity activity;

    public LoginHelper(LoginResultListener loginResultListener, FragmentActivity activity) {
        this.loginResultListener = loginResultListener;
        this.activity = activity;
    }

    public void silentLogin() {
        if(PropertiesManager.isSignedInWithEmail())
            loginWithEmail();
        else if(PropertiesManager.isSignedInWithFacebook())
            loginWithFacebook();
        else if(PropertiesManager.isSignedInWithGoogle())
            loginWithGoogle();
        else if(PropertiesManager.getUserId() != null)
            loginAnonymously();
        else
            createAnonymousAccount();
    }

    public void loginWithEmail() {
        Log.d(TAG, "Trying to auto log in with email and password");
        final String email = PropertiesManager.getEmail();
        final String password = PropertiesManager.getPassword();
        try {
            LypeActivitiesApiService.getInstance(activity).getApi().postLogin(email, password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "postLogin onCompleted");
                        }

                        @Override
                        public void onError(final Throwable e) {
                            Log.e(TAG, "postLogin onError");
                            e.printStackTrace();
                            loginResultListener.onLoginFailed(R.string.login_sign_in_failed);
                        }

                        @Override
                        public void onNext(final String jsonResponse) {
                            Log.d(TAG, "onNext: " + jsonResponse);
                            try {
                                JSONObject json = new JSONObject(jsonResponse);
                                JSONObject data = json.getJSONObject("data");
                                String sessionKey = data.getString("sessionKey");
                                PropertiesManager.setSessionKey(sessionKey);
                                if(data.has("user")) {
                                    JSONObject user = data.getJSONObject("user");
                                    PropertiesManager.setUser(user);
                                    String name = user.getString("name");
                                    loginResultListener.onLoginSuccessful(name);
                                } else {
                                    Log.e(TAG, "Missing \"user\" tag in JSON response.");
                                    loginResultListener.onLoginSuccessful(null);
                                }
                                PropertiesManager.saveProperties();
                            } catch (Exception e) {
                                e.printStackTrace();
                                loginResultListener.onLoginFailed(R.string.login_email_fail);
                            }
                        }
                    });
        } catch (Exception e) {
            Log.e(TAG, "Failed to log in with email");
            e.printStackTrace();
            loginResultListener.onLoginFailed(R.string.login_sign_in_failed);
        }
    }

    private void loginAnonymously() {
        Log.d(TAG, "Trying to anonymously auto log in with user id");
        String userId = PropertiesManager.getUserId();
        try {
            LypeActivitiesApiService.getInstance(activity).getApi().postAnonymousLogin(userId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "postAnonymousLogin onCompleted");
                        }

                        @Override
                        public void onError(final Throwable e) {
                            Log.e(TAG, "postAnonymousLogin onError");
                            e.printStackTrace();
                            loginResultListener.onLoginFailed(0);
                        }

                        @Override
                        public void onNext(final String jsonResponse) {
                            Log.d(TAG, "onNext: " + jsonResponse);
                            try {
                                JSONObject json = new JSONObject(jsonResponse);
                                JSONObject data = json.getJSONObject("data");
                                String sessionKey = data.getString("sessionKey");
                                PropertiesManager.setSessionKey(sessionKey);
                                PropertiesManager.saveProperties();
                                Log.d(TAG, "Anonymous auto log in was successful");
                                loginResultListener.onLoginSuccessful(null);
                            } catch (Exception e) {
                                e.printStackTrace();
                                loginResultListener.onLoginFailed(0);
                            }
                        }
                    });
        } catch (Exception e) {
            Log.e(TAG, "Failed to auto log in anonymously");
            e.printStackTrace();
            loginResultListener.onLoginFailed(0);
        }
    }

    private void createAnonymousAccount() {
        String GUID = PropertiesManager.getGUID();
        if(GUID == null || GUID.isEmpty())
            GUID = PropertiesManager.generateGUID();
        try {
            LypeActivitiesApiService.getInstance(activity).getApi().postAnonymousUser(GUID)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "postAnonymousUser onCompleted");
                        }

                        @Override
                        public void onError(final Throwable e) {
                            Log.e(TAG, "postAnonymousUser onError");
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(final String jsonResponse) {
                            Log.d(TAG, "onNext: " + jsonResponse);
                            try {
                                JSONObject json = new JSONObject(jsonResponse);
                                JSONObject user = json.getJSONObject("data");
                                PropertiesManager.setUser(user);
                                PropertiesManager.saveProperties();
                                Log.d(TAG, "Anonymous user created successfully");
                                loginAnonymously();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loginWithFacebook() {
        facebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook login onSuccess");
                try {
                    JSONObject body = new JSONObject();
                    body.put("type", "mobile");
                    body.put("token", loginResult.getAccessToken().getToken());
                    body.put("deviceid", PropertiesManager.getGUID());
                    body.put("userid", PropertiesManager.getUserId());
                    LypeActivitiesApiService.getInstance(activity).getApi().postFacebookLogin(body)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Subscriber<String>() {
                                @Override
                                public void onCompleted() {
                                    Log.d(TAG, "postFacebookLogin onCompleted");
                                }

                                @Override
                                public void onError(final Throwable e) {
                                    e.printStackTrace();
                                    loginResultListener.onLoginFailed(R.string.login_sign_in_failed);
                                    LoginManager.getInstance().logOut();
                                }

                                @Override
                                public void onNext(final String jsonResponse) {
                                    Log.d(TAG, "onNext: " + jsonResponse);
                                    try {
                                        JSONObject json = new JSONObject(jsonResponse);
                                        JSONObject data = json.getJSONObject("data");
                                        String sessionKey = data.getString("sessionKey");
                                        JSONObject user = data.getJSONObject("user");
                                        PropertiesManager.setSessionKey(sessionKey);
                                        PropertiesManager.setFacebookSignIn(true);
                                        PropertiesManager.setUser(user);
                                        PropertiesManager.saveProperties();
                                        loginResultListener.onLoginSuccessful(user.getString("name"));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        loginResultListener.onLoginFailed(R.string.login_facebook_fail);
                                    }
                                }
                            });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook login onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                loginResultListener.onLoginFailed(R.string.login_facebook_fail);
            }
        });
    }

    public void loginWithGoogle() {
        if(googleApiClient == null) {
            // Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestProfile()
                    .requestEmail()
                    .requestId()
                    .requestIdToken(activity.getString(R.string.google_oauth_id))
                    .build();
            googleApiClient = new GoogleApiClient.Builder(activity)
                    .enableAutoManage(activity, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void manageActivityResult(final int requestCode, final int resultCode, final Intent data) {
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        } else {
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleGoogleSignInResult(final GoogleSignInResult result) {
        Log.d(TAG, "handleGoogleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if(acct != null) {
                Log.d(TAG, acct.getGivenName());
                Log.d(TAG, acct.getFamilyName());
                Log.d(TAG, acct.getDisplayName());
                Log.d(TAG, acct.getEmail());
                Log.d(TAG, acct.getId());
                Log.d(TAG, acct.getIdToken());
                //Log.d(TAG, acct.getServerAuthCode());
            }
            try {
                JSONObject body = new JSONObject();
                body.put("type", "mobile");
                body.put("id_token", acct.getIdToken());
                body.put("deviceid", PropertiesManager.getGUID());
                body.put("userid", PropertiesManager.getUserId());
                LypeActivitiesApiService.getInstance(activity).getApi().postGoogleLogin(body)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "postGoogleLogin onCompleted");
                            }

                            @Override
                            public void onError(final Throwable e) {
                                e.printStackTrace();
                                loginResultListener.onLoginFailed(R.string.login_sign_in_failed);
                            }

                            @Override
                            public void onNext(final String jsonResponse) {
                                Log.d(TAG, "onNext: " + jsonResponse);
                                try {
                                    JSONObject json = new JSONObject(jsonResponse);
                                    JSONObject data = json.getJSONObject("data");
                                    String sessionKey = data.getString("sessionKey");
                                    JSONObject user = data.getJSONObject("user");
                                    PropertiesManager.setSessionKey(sessionKey);
                                    PropertiesManager.setGoogleSignIn(true);
                                    PropertiesManager.setUser(user);
                                    PropertiesManager.saveProperties();
                                    loginResultListener.onLoginSuccessful(user.getString("name"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    loginResultListener.onLoginFailed(R.string.login_google_fail);
                                }
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
                loginResultListener.onLoginFailed(R.string.login_sign_in_failed);
            }
        } else {
            loginResultListener.onLoginFailed(R.string.login_google_fail);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
    }
}

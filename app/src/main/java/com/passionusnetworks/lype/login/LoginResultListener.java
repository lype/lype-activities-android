package com.passionusnetworks.lype.login;

/**
 * Created by Raphael on 2017-05-25.
 */

public interface LoginResultListener {
    void onLoginSuccessful(String username);
    void onLoginFailed(int stringId);
}

package com.passionusnetworks.lype;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Raphael on 2016-10-20.
 */

public class LypeApplication extends Application {

    @Override
    public void onCreate() {
        Log.d("LypeApplication", "onCreate");
        Fabric.with(this, new Crashlytics());
        super.onCreate();
    }
}

package com.passionusnetworks.lype.services.http;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.models.ActivitySuggestion;
import com.passionusnetworks.lype.models.Category;
import com.passionusnetworks.lype.models.Comment;
import com.passionusnetworks.lype.models.Filter;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.models.LypeActivity;
import com.passionusnetworks.lype.models.User;
import com.passionusnetworks.lype.persistence.PropertiesManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Raphael on 2016-03-03.
 */
public class LypeActivitiesApiService {
    private static final String TAG = "LypeActivitiesApiSer...";
    public static final int PAGE_COUNT = 15;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType IMAGE = MediaType.parse("image/png");
    private static LypeActivitiesApiService instance;
    private Context context;
    private LypeActivitiesApi api;
    private LypeActivitiesApi mockedApi;
    public static String apiHost = "http://lype.co:4000/api";
    //public static String apiHost = "http://135.19.170.240:4000/api";
    //public static String apiHost = "http://192.168.10.130:4000/api";

    public static LypeActivitiesApiService getInstance(Context context) {
        if(instance == null)
            instance = new LypeActivitiesApiService(context);
        return instance;
    }

    private LypeActivitiesApiService(final Context context) {
        this.context = context;
        initApi();
        initMockedApi();
    }

    private void initApi() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        String appName = context.getString(context.getApplicationInfo().labelRes);
                        Request.Builder requestBuilder = original.newBuilder()
                                .header("X-App-Name", appName)
                                .header("Accept-Version", "1")
                                .method(original.method(), original.body());
                        String sessionKey = PropertiesManager.getSessionKey();
                        if (sessionKey != null)
                            requestBuilder.header("X-Session-Key", sessionKey);
                        Request request = requestBuilder.build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        api = new LypeActivitiesApi() {
            @Override
            public Observable<List<String>> getMoments() {
                return null;
            }

            @Override
            public Observable<String> getMeteo(String when, Location where) {
                return null;
            }

            @Override
            public Observable<List<Category>> getCategories() {
                final Request request = createGetRequestAt("/categories?lang="+ Locale.getDefault().getLanguage());
                return Observable.create(new Observable.OnSubscribe<List<Category>>() {
                    @Override
                    public void call(Subscriber<? super List<Category>> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject json = new JSONObject(response.body().string());
                                subscriber.onNext(Category.categoriesFromJsonArray(json.getJSONArray("data"), context).subList(0, 6));
                            } else {
                                subscriber.onNext(new ArrayList<Category>());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<List<LypeActivity>> getActivities(Location location, List<FilterType> filters, int pageOffset) {
                String url = String.format("/activities?current_location=%s,%s&offset=%s&count=%s", location.getLatitude(), location.getLongitude(), pageOffset * PAGE_COUNT, PAGE_COUNT);
                if(filters != null) {
                    for(FilterType filterType : filters) {
                        url += "&tag_" + filterType.getType() + "=";
                        boolean first = true;
                        for(Filter filter : filterType.getFilters(context)) {
                            if(!first)
                                url += "|";
                            else
                                first = false;
                            url += filter.getValue();
                        }
                    }
                }
                final Request request = createGetRequestAt(url);
                return Observable.create(new Observable.OnSubscribe<List<LypeActivity>>() {
                    @Override
                    public void call(Subscriber<? super List<LypeActivity>> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject json = new JSONObject(response.body().string());
                                subscriber.onNext(LypeActivity.activitiesFromJsonArray(json.getJSONArray("data"), context));
                            } else {
                                subscriber.onNext(new ArrayList<LypeActivity>());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<List<LypeActivity>> getSuggestions(int offset) {
                final Request request = createGetRequestAt(String.format("/activities/pending?offset=%s&count=%s", offset, PAGE_COUNT));
                return Observable.create(new Observable.OnSubscribe<List<LypeActivity>>() {
                    @Override
                    public void call(Subscriber<? super List<LypeActivity>> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject json = new JSONObject(response.body().string());
                                subscriber.onNext(LypeActivity.activitiesFromJsonArray(json.getJSONArray("data"), context));
                            } else {
                                subscriber.onNext(new ArrayList<LypeActivity>());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<List<FilterType>> getFilters() {
                return null;
            }

            @Override
            public Observable<List<LypeActivity>> getFavorites(int offset) {
                final Request request = createGetRequestAt(String.format("/activities/favorites?offset=%s&count=%s", offset * PAGE_COUNT, PAGE_COUNT));
                return Observable.create(new Observable.OnSubscribe<List<LypeActivity>>() {
                    @Override
                    public void call(Subscriber<? super List<LypeActivity>> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject json = new JSONObject(response.body().string());
                                subscriber.onNext(LypeActivity.activitiesFromJsonArray(json.getJSONArray("data"), context));
                            } else {
                                subscriber.onNext(new ArrayList<LypeActivity>());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<List<Comment>> getComments(String activityId) {
                final Request request = createGetRequestAt(String.format("/activities/%s/comments", activityId));
                return Observable.create(new Observable.OnSubscribe<List<Comment>>() {
                    @Override
                    public void call(Subscriber<? super List<Comment>> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject json = new JSONObject(response.body().string());
                                JSONArray data = json.getJSONArray("data");
                                subscriber.onNext(Comment.fromJson(data));
                            } else {
                                subscriber.onNext(new ArrayList<Comment>());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> postSuggestion(ActivitySuggestion activitySuggestion) throws JSONException, IOException {
                JSONObject json = new JSONObject();
                //What
                json.put("name", activitySuggestion.getName());
                json.put("description", activitySuggestion.getDescription());
                json.put("website", activitySuggestion.getWebsite());
                //When
                JSONObject jsonLocation = new JSONObject();
                jsonLocation.put("latitude", activitySuggestion.getLocation().latitude);
                jsonLocation.put("longitude", activitySuggestion.getLocation().longitude);
                jsonLocation.put("address", activitySuggestion.getAddress());
                json.put("location", jsonLocation);
                //Tags
                json.put("tags", FilterType.jsonObjectFromFilterTypes(activitySuggestion.getTags()));
                //Author
                json.put("author", PropertiesManager.getUserId());

                RequestBody jsonBody = RequestBody.create(JSON, json.toString());

                final Request request = createPostAt("/activities", jsonBody);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Log.i(TAG, "Sending suggestion request");
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                JSONObject data = jsonResponse.getJSONObject("data");
                                String id = data.getString("id");
                                subscriber.onNext(id);
                            } else {
                                Log.e(TAG, "response was not successful");
                                subscriber.onNext(null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> putSuggestionImage(ActivitySuggestion activitySuggestion) {
                File imageFile = activitySuggestion.getImageFile(context);
                RequestBody imageBody = RequestBody.create(IMAGE, imageFile);
                RequestBody body = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("picture", "activity_image.png", imageBody)
                        .build();
                String route = String.format("/activities/%s/pictures", activitySuggestion.getId());
                final Request request = createPutAt(route, body);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Log.i(TAG, "Sending suggestion picture");
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                subscriber.onNext(jsonResponse.toString());
                            } else {
                                Log.e(TAG, "response was not successful");
                                subscriber.onNext(null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> postLogin(String email, String password) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("email", email);
                json.put("password", password);
                json.put("type", "mobile");
                return postLogin(json);
            }

            @Override
            public Observable<String> postAnonymousLogin(String userId) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("userid", userId);
                json.put("type", "mobile");
                return postLogin(json);
            }

            @Override
            public Observable<String> postLogin(JSONObject body) throws JSONException {
                RequestBody requestBody = RequestBody.create(JSON, body.toString());
                final Request request = createPostAt("/sessions", requestBody);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                subscriber.onNext(jsonResponse.toString());
                            } else {
                                subscriber.onError(new Exception("Authentication failed"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> postGoogleLogin(JSONObject body) throws JSONException {
                RequestBody requestBody = RequestBody.create(JSON, body.toString());
                final Request request = createPostAt("/sessions/oauth/google", requestBody);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                subscriber.onNext(jsonResponse.toString());
                            } else {
                                subscriber.onError(new Exception("Authentication failed"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> postFacebookLogin(JSONObject body) throws JSONException {
                RequestBody requestBody = RequestBody.create(JSON, body.toString());
                final Request request = createPostAt("/sessions/oauth/facebook", requestBody);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                subscriber.onNext(jsonResponse.toString());
                            } else {
                                subscriber.onError(new Exception("Authentication failed"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> postCreateAccount(String name, String email, String password, String GUID) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("name", name);
                json.put("email", email);
                json.put("password", password);
                json.put("deviceid", GUID);
                RequestBody body = RequestBody.create(JSON, json.toString());
                final Request request = createPostAt("/users", body);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                subscriber.onNext(jsonResponse.toString());
                            } else if(response.code() == 409) {
                                String responseText = response.body().string();
                                Log.i(TAG, responseText);
                                subscriber.onNext(responseText);
                            } else {
                                subscriber.onError(new Exception("Failed to create account"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<String> postAnonymousUser(String GUID) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("deviceid", GUID);
                json.put("anonymous", true);
                RequestBody body = RequestBody.create(JSON, json.toString());
                final Request request = createPostAt("/users", body);
                return Observable.create(new Observable.OnSubscribe<String>() {
                    @Override
                    public void call(Subscriber<? super String> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                Log.i(TAG, jsonResponse.toString());
                                subscriber.onNext(jsonResponse.toString());
                            } else {
                                subscriber.onError(new Exception("Failed to create anonymous user"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<Boolean> putActivityFeedback(String activityId, boolean interested) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("vote", interested ? 1 : 0);
                RequestBody body = RequestBody.create(JSON, json.toString());
                String route = String.format("/activities/%s/vote", activityId);
                final Request request = createPutAt(route, body);
                return Observable.create(new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(Subscriber<? super Boolean> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                Log.i(TAG, "PUT /vote Successful");
                                subscriber.onNext(true);
                            } else {
                                subscriber.onError(new Exception("PUT /vote Error "+response.code()+": "+response.message()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<Boolean> putSuggestionFeedback(String activityId, boolean approved) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("approved", approved);
                RequestBody body = RequestBody.create(JSON, json.toString());
                String route = String.format("/activities/%s/update-approval", activityId);
                final Request request = createPutAt(route, body);
                return Observable.create(new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(Subscriber<? super Boolean> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                Log.i(TAG, "POST /update-approval Successful");
                                subscriber.onNext(true);
                            } else {
                                subscriber.onError(new Exception("POST /update-approval Error "+response.code()+": "+response.message()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }

            @Override
            public Observable<Comment> postComment(String activityId, String title, String text) throws JSONException {
                JSONObject json = new JSONObject();
                json.put("title", title);
                json.put("text", text);
                RequestBody body = RequestBody.create(JSON, json.toString());
                String route = String.format("/activities/%s/comments", activityId);
                final Request request = createPostAt(route, body);
                return Observable.create(new Observable.OnSubscribe<Comment>() {
                    @Override
                    public void call(Subscriber<? super Comment> subscriber) {
                        subscriber.onStart();
                        try {
                            Response response = okHttpClient.newCall(request).execute();
                            if (response.isSuccessful()) {
                                Log.i(TAG, "POST /comments Successful");
                                JSONObject jsonResponse = new JSONObject(response.body().string());
                                JSONArray data = jsonResponse.getJSONArray("data");
                                Comment comment  = Comment.fromJson(data.getJSONObject(0));
                                subscriber.onNext(comment);
                            } else {
                                subscriber.onError(new Exception("POST /comments Error "+response.code()+": "+response.message()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                        subscriber.onCompleted();
                    }
                });
            }
        };
    }

    private Request createGetRequestAt(String route) {
        return new Request.Builder()
                .url(apiHost+route)
                .get()
                .build();
    }

    private Request createPostAt(String route, RequestBody body) {
        return new Request.Builder()
                .url(apiHost+route)
                .post(body)
                .build();
    }

    private Request createPutAt(String route, RequestBody body) {
        return new Request.Builder()
                .url(apiHost+route)
                .put(body)
                .build();
    }

    private void initMockedApi() {
        mockedApi = new LypeActivitiesApi() {
            @Override
            public Observable<List<String>> getMoments() {
                List<String> moments = new ArrayList<>();
                moments.add("today");
                moments.add("tomorrow");
                moments.add("next weekend");
                moments.add("next week");
                moments.add("Enter date...");
                return Observable.just(moments);
            }

            @Override
            public Observable<String> getMeteo(String when, Location where) {
                if(when.equals("tonight"))
                    return Observable.just("ic_weather_night_white_48dp");
                return Observable.just("ic_weather_sunny_white_48dp");
            }

            @Override
            public Observable<List<Category>> getCategories() {
                /*List<Category> categories = new ArrayList<>();
                categories.add(new Category("Shop with friends", "https://d30y9cdsu7xlg0.cloudfront.net/png/28468-200.png"));
                categories.add(new Category("Relax at the beach", "http://icons.iconarchive.com/icons/icons8/ios7/512/Weather-Sun-icon.png"));
                categories.add(new Category("Eat", "http://icons.iconarchive.com/icons/icons8/windows-8/512/Ecommerce-Food-icon.png"));
                categories.add(new Category("Watch a movie", "http://simpleicon.com/wp-content/uploads/movie-1.png"));
                categories.add(new Category("Enjoy an event", "http://r3dpd2rd6uu3tdzyruozp27u.wpengine.netdna-cdn.com/wp-content/themes/buddyboss-child/images/event-avatar.png"));
                categories.add(new Category("Go on a road trip", "http://icons.iconarchive.com/icons/iconsmind/outline/512/Car-2-icon.png"));
                categories.add(new Category("I could do anything!", null));
                categories.add(new Category("More...", null));
                return Observable.just(categories);*/
                return null;
            }

            @Override
            public Observable<List<LypeActivity>> getSuggestions(int offset) {
                return null;
            }

            @Override
            public Observable<List<LypeActivity>> getActivities(Location location, List<FilterType> filters, int offset) {
                List<LypeActivity> activities = new ArrayList<>();
                for(int i=0; i<10; i++) {
                    location.setLatitude(45.514666);
                    location.setLongitude(-73.643888);
                    ArrayList<Comment> comments = new ArrayList<>();
                    String image = i % 2 == 0 ?
                            "http://d1xejl9xcsndu9.cloudfront.net/wp-content/uploads/2011/05/field-museum-636x431.jpg" :
                            "http://www.visitmelbourne.com/~/media/images/great-ocean-road/things-to-do/outdoor-activities/walking-and-hiking/great-ocean-walk-couple_gor_r_1374425_1150x863.jpg?ts=20150828340216&w=480&h=360&crop=1%7C/~/media/images/great-ocean-road/things-to-do/outdoor-activities/walking-and-hiking/great-ocean-walk-couple_gor_r_1374425_1150x863.jpg?ts=20150828340216&w=720&h=540&crop=1";
                    activities.add(new LypeActivity(
                            "0",
                            "Activity " + (i + 1),
                            image,
                            getFilterSample(),
                            context.getString(R.string.lorem_ipsum),
                            "http://google.ca",
                            "Fake address",
                            location,
                            null,
                            null,
                            comments));
                }
                return Observable.just(activities);
            }

            @Override
            public Observable<List<FilterType>> getFilters() {
                return Observable.just(FilterType.getAllFilters(context));
            }

            @Override
            public Observable<List<LypeActivity>> getFavorites(int offset) {
                return getActivities(null, null, 0);
            }

            @Override
            public Observable<List<Comment>> getComments(String activityId) {
                ArrayList<Comment> comments = new ArrayList<>();
                User user = new User("", "Anonymous", "http://www.anonymous-france.eu/IMG/siteon0.png?1459607225");
                comments.add(new Comment("", "Awesome", "I went with my girlfriend, it was awesome.", user));
                comments.add(new Comment("", "Perfect", "10/10 would do again.", user));
                comments.add(new Comment("", "Correct", "Meh... I've seen better", user));
                return Observable.just((List<Comment>) comments);
            }

            @Override
            public Observable<String> postSuggestion(ActivitySuggestion activitySuggestion) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> putSuggestionImage(ActivitySuggestion activitySuggestion) {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postLogin(String email, String password) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postAnonymousLogin(String userId) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postLogin(JSONObject body) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postGoogleLogin(JSONObject body) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postFacebookLogin(JSONObject body) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postCreateAccount(String name, String email, String password, String GUID) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<String> postAnonymousUser(String GUID) throws JSONException {
                return Observable.just("success");
            }

            @Override
            public Observable<Boolean> putActivityFeedback(String activitId, boolean interested) throws JSONException {
                return Observable.just(true);
            }

            @Override
            public Observable<Boolean> putSuggestionFeedback(String activitId, boolean approved) throws JSONException {
                return Observable.just(true);
            }

            @Override
            public Observable<Comment> postComment(String activitId, String title, String text) throws JSONException {
                return Observable.just(new Comment("", "Title", "Text", new User("", "Username", null)));
            }
        };
    }

    private List<FilterType> getFilterSample() {
        List<FilterType> filterTypes = new ArrayList<>();
        List<Filter> timeFilters = new ArrayList<>();
        timeFilters.add(new Filter("morning", R.drawable.ic_schedule_white_24dp, "Morning"));
        filterTypes.add(new FilterType(FilterType.FILTER_TYPE_TIME, R.drawable.ic_schedule_white_24dp, context.getResources().getString(R.string.filter_type_time), timeFilters));
        List<Filter> priceFilters = new ArrayList<>();
        priceFilters.add(new Filter("free", R.drawable.ic_monetization_on_white_24dp, "Free"));
        filterTypes.add(new FilterType(FilterType.FILTER_TYPE_PRICE, R.drawable.ic_monetization_on_white_24dp, context.getResources().getString(R.string.filter_type_price), priceFilters));
        List<Filter> socialFilters = new ArrayList<>();
        socialFilters.add(new Filter("alone", R.drawable.ic_people_white_24dp, "Alone"));
        filterTypes.add(new FilterType(FilterType.FILTER_TYPE_SOCIAL, R.drawable.ic_people_white_24dp, context.getResources().getString(R.string.filter_type_social), socialFilters));
        List<Filter> distanceFilters = new ArrayList<>();
        distanceFilters.add(new Filter("very_close", R.drawable.ic_place_white_24dp, "Very close"));
        filterTypes.add(new FilterType(FilterType.FILTER_TYPE_DISTANCE, R.drawable.ic_place_white_24dp, context.getResources().getString(R.string.filter_type_distance), distanceFilters));
        List<Filter> passionFilters = new ArrayList<>();
        passionFilters.add(new Filter("sport", R.drawable.ic_favorite_border_white_24dp, "Sport"));
        filterTypes.add(new FilterType(FilterType.FILTER_TYPE_PASSION, R.drawable.ic_favorite_border_white_24dp, context.getResources().getString(R.string.filter_type_passion), passionFilters));
        return filterTypes;
    }

    public LypeActivitiesApi getApi() {
        return api;
    }

    public LypeActivitiesApi getMockedApi() {
        return mockedApi;
    }
}

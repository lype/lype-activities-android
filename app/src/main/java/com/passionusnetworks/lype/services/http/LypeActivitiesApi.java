package com.passionusnetworks.lype.services.http;

import android.location.Location;

import com.passionusnetworks.lype.models.ActivitySuggestion;
import com.passionusnetworks.lype.models.Category;
import com.passionusnetworks.lype.models.Comment;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.models.LypeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import rx.Observable;

/**
 * Created by Raphael on 2016-03-03.
 */
public interface LypeActivitiesApi {
    Observable<List<String>> getMoments();
    Observable<String> getMeteo(String when, Location where);
    Observable<List<Category>> getCategories();
    Observable<List<LypeActivity>> getActivities(Location location, List<FilterType> filters, int pageOffset);
    Observable<List<LypeActivity>> getSuggestions(int offset);
    Observable<List<FilterType>> getFilters();
    Observable<List<LypeActivity>> getFavorites(int offset);
    Observable<List<Comment>> getComments(String activityId);
    Observable<String> postSuggestion(ActivitySuggestion activitySuggestion) throws JSONException, IOException;
    Observable<String> putSuggestionImage(ActivitySuggestion activitySuggestion);
    Observable<String> postLogin(String email, String password) throws JSONException;
    Observable<String> postAnonymousLogin(String userId) throws JSONException;
    Observable<String> postLogin(JSONObject body) throws JSONException;
    Observable<String> postGoogleLogin(JSONObject body) throws JSONException;
    Observable<String> postFacebookLogin(JSONObject body) throws JSONException;
    Observable<String> postCreateAccount(String name, String email, String password, String GUID) throws JSONException;
    Observable<String> postAnonymousUser(String GUID) throws JSONException;
    Observable<Boolean> putActivityFeedback(String activityId, boolean interested) throws JSONException;
    Observable<Boolean> putSuggestionFeedback(String activityId, boolean approved) throws JSONException;
    Observable<Comment> postComment(String activityId, String title, String text) throws JSONException;
}

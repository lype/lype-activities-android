package com.passionusnetworks.lype.persistence;

import android.content.Context;

/**
 * Created by Raphael on 2017-03-30.
 */

public interface DatabaseObject {
    long save(Context context);
    boolean delete(Context context);
}

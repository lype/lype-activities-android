package com.passionusnetworks.lype.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.passionusnetworks.lype.models.Filter;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.models.LypeActivity;

/**
 * Created by Raphael on 2017-03-30.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "lype";

    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //execute the CREATE script for every table
        sqLiteDatabase.execSQL(LypeActivity.CREATE_QUERY);
        sqLiteDatabase.execSQL(FilterType.CREATE_QUERY);
        sqLiteDatabase.execSQL(Filter.CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

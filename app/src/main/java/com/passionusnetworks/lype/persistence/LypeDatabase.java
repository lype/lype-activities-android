package com.passionusnetworks.lype.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.passionusnetworks.lype.models.Filter;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.models.LypeActivity;

/**
 * Created by Raphael on 2017-03-30.
 */

public class LypeDatabase {

    private DatabaseHelper helper;
    private SQLiteDatabase db;

    private static LypeDatabase instance;

    public static synchronized LypeDatabase getInstance(Context context) {
        if(instance == null)
            instance = new LypeDatabase(context);
        return instance;
    }

    private LypeDatabase(Context context) {
        helper = new DatabaseHelper(context);
        db = helper.getWritableDatabase();
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void clearDatabase() {
        db.delete(Filter.TABLE_NAME, null, null);
        db.delete(FilterType.TABLE_NAME, null, null);
        db.delete(LypeActivity.TABLE_NAME, null, null);
    }
}

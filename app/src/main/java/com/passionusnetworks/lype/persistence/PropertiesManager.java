package com.passionusnetworks.lype.persistence;

import android.content.Context;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.Profile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by Raphael Royer-Rivard on 2015-03-25.
 */
public class PropertiesManager {
    private static final String TAG = "PropertiesManager";
    private static final String PROPERTIES_PATH = "com.passionusnetworks.lype.properties";

    private static Properties properties = new Properties();
    private static Context context;

    private PropertiesManager(){}

    public static synchronized boolean loadProperties(final Context context){
        if(PropertiesManager.context != null)
            return true;
        PropertiesManager.context = context;
        Thread t = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    Log.v(TAG, "load properties");
                    properties.clear();
                    properties.load(context.openFileInput(PROPERTIES_PATH));
                    Log.v(TAG, "properties successfuly loaded");
                } catch (FileNotFoundException ex) {
                    Log.v(TAG, "create default properties");
                    createDefaultPropertiesFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static boolean createDefaultPropertiesFile(){
        try {
            properties = new Properties();
            properties.store(context.openFileOutput(PROPERTIES_PATH, Context.MODE_PRIVATE), null);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean saveProperties(){
        try {
            properties.store(context.openFileOutput(PROPERTIES_PATH, Context.MODE_APPEND), null);
            return true;
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public static boolean isSignedIn() {
        return isSignedInWithEmail() || isSignedInWithFacebook() || isSignedInWithGoogle();
    }

    public static boolean isSignedInWithEmail() {
        String sessionKey = getSessionKey();
        String email = getEmail();
        String password = getPassword();
        return sessionKey != null && !sessionKey.isEmpty() && email != null && !email.isEmpty() && password != null && !password.isEmpty();
    }

    public static void setFacebookSignIn(boolean signedIn) {
        properties.setProperty("facebook_sign_in", String.valueOf(signedIn));
    }

    public static boolean isSignedInWithFacebook() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        Profile profile = Profile.getCurrentProfile();
        boolean facebookSignIn = Boolean.valueOf(properties.getProperty("facebook_sign_in", "false"));
        return accessToken != null && !accessToken.isExpired() && profile != null && facebookSignIn;
    }

    public static void setGoogleSignIn(boolean signedIn) {
        properties.setProperty("google_sign_in", String.valueOf(signedIn));
    }

    public static boolean isSignedInWithGoogle() {
        return Boolean.valueOf(properties.getProperty("google_sign_in", "false"));
    }

    public static String getSessionKey() {
        return properties.getProperty("session_key", null);
    }

    public static void setSessionKey(String sessionKey) {
        properties.setProperty("session_key", sessionKey);
    }

    public static String getGUID() {
        return properties.getProperty("GUID", null);
    }

    public static String generateGUID() {
        String GUID = UUID.randomUUID().toString();
        properties.setProperty("GUID", GUID);
        saveProperties();
        return GUID;
    }

    public static String getEmail() {
        return properties.getProperty("email", null);
    }

    public static void setEmail(String email) {
        properties.setProperty("email", email);
    }

    public static String getPassword() {
        return properties.getProperty("password", null);
    }

    public static void setPassword(String password) {
        properties.setProperty("password", password);
    }

    private static JSONObject getUser() throws JSONException {
        String user = properties.getProperty("user");
        return user != null ? new JSONObject(user) : new JSONObject();
    }

    public static void setUser(JSONObject user) {
        properties.setProperty("user", user.toString());
    }

    public static String getUserId() {
        try {
            JSONObject user = getUser();
            return user.getString("id");
        } catch (JSONException e) {
            Log.w(TAG, "No userid stored in properties.");
            //e.printStackTrace();
        }
        return null;
    }

    public static String getUserName() {
        try {
            JSONObject user = getUser();
            return user.getString("first_name") + " " + user.getString("last_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getProfilePicture() {
        try {
            JSONObject user = getUser();
            return user.getString("picture");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getProfileBackgroundPicture() {
        try {
            JSONObject user = getUser();
            return user.getString("bgPicture");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isUserModerator() {
        try {
            JSONObject user = getUser();
            return user.getString("type").equalsIgnoreCase("moderator");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void clearLoginInformation() {
        clearUser();
        clearSessionKey();
        clearPassword();
        clearEmail();
        setFacebookSignIn(false);
        setGoogleSignIn(false);
    }

    public static void clearUser() {
        setUser(new JSONObject());
    }

    public static void clearEmail() {
        setEmail("");
    }

    public static void clearPassword() {
        setPassword("");
    }

    public static void clearSessionKey() {
        setSessionKey("");
    }

    public static void clearUUID() {
        properties.setProperty("GUID", "");
    }

    public static boolean isNewUser() {
        return Boolean.parseBoolean(properties.getProperty("newUser", "true"));
    }

    public static void setNewUser(boolean newUser) {
        properties.setProperty("newUser", String.valueOf(newUser));
    }
}

package com.passionusnetworks.lype.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.activities.SuggestionActivity;
import com.passionusnetworks.lype.models.ActivitySuggestion;

/**
 * Created by Raphael on 2016-04-18.
 */
public class SuggestionWhereFragment extends SuggestionFragment implements OnMapReadyCallback {
    private static final String TAG = "SuggestionWhereFragment";
    private static final String ADDRESS_SAVED_INSTANCE_STATE = "address";
    private static final String LATITUDE_SAVED_INSTANCE_STATE = "latitude";
    private static final String LONGITUDE_SAVED_INSTANCE_STATE = "longitude";
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;

    private CardView addressCard;
    private TextView address;
    private GoogleMap map;
    private Marker marker;
    private ActivitySuggestion activitySuggestion;
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_suggestion_where, container, false);
        initLayout(root);

        if(savedInstanceState != null) {
            Log.d(TAG, "savedInstanceState is not null");
            String savedAddress = savedInstanceState.getString(ADDRESS_SAVED_INSTANCE_STATE);
            double lat = savedInstanceState.getDouble(LATITUDE_SAVED_INSTANCE_STATE);
            double lon = savedInstanceState.getDouble(LONGITUDE_SAVED_INSTANCE_STATE);
            if(savedAddress != null && lat != 0 && lon != 0)
                updateAddress(savedAddress, null, new LatLng(lat, lon));
        }

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        if(!address.getText().toString().isEmpty())
            outState.putString(ADDRESS_SAVED_INSTANCE_STATE, address.getText().toString());
        if(marker != null) {
            outState.putDouble(LATITUDE_SAVED_INSTANCE_STATE, marker.getPosition().latitude);
            outState.putDouble(LONGITUDE_SAVED_INSTANCE_STATE, marker.getPosition().longitude);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(root != null)
            root.findViewById(R.id.suggestion_search_address).setEnabled(true);
    }

    public void updateAddress(Place place) {
        updateAddress(place.getAddress().toString(), place.getName().toString(), place.getLatLng());
    }

    private void updateAddress(String placeAddress, String placeName, LatLng position) {
        address.setText(placeAddress);
        if(map != null) {
            if (marker != null)
                marker.remove();

            marker = map.addMarker(new MarkerOptions().position(position));

            if(placeName != null)
                marker.setTitle(placeName);

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
            //map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        }
        addressCard.setVisibility(View.VISIBLE);
        save(activitySuggestion);
    }

    private void findPlace() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
            getActivity().startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
            e.printStackTrace();
        }
    }

    private void initLayout(View root) {
        final Button search = (Button) root.findViewById(R.id.suggestion_search_address);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search.setEnabled(false);
                findPlace();
            }
        });
        address = (TextView) root.findViewById(R.id.suggestion_address);
        addressCard = (CardView) root.findViewById(R.id.address_card);
        addressCard.setVisibility(View.INVISIBLE);
        SupportMapFragment mapFragment = new SupportMapFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.suggestion_map, mapFragment).commit();
        mapFragment.getMapAsync(this);
    }

    private void load() {
        ActivitySuggestion activitySuggestion = ((SuggestionActivity) getActivity()).getActivitySuggestion();
        if(activitySuggestion != null) {
            this.activitySuggestion = activitySuggestion;
            if (activitySuggestion.getAddress() != null && activitySuggestion.getLocation() != null)
                updateAddress(activitySuggestion.getAddress(), null, activitySuggestion.getLocation());
        }
    }

    @Override
    public boolean validate() {
        if(address.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), R.string.suggestion_validation_address, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void save(ActivitySuggestion activitySuggestion) {
        activitySuggestion.setAddress(address.getText().toString());
        activitySuggestion.setLocation(marker.getPosition());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        load();
    }
}

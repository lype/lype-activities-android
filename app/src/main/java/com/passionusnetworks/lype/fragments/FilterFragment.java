package com.passionusnetworks.lype.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.adapters.FilterAdapter;
import com.passionusnetworks.lype.itemdecorations.VerticalSpaceItemDecoration;
import com.passionusnetworks.lype.models.ActivitySuggestion;
import com.passionusnetworks.lype.models.Filter;
import com.passionusnetworks.lype.models.FilterType;

import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class FilterFragment extends SuggestionFragment {
    private static final String TAG = "FilterFragment";
    private static final String FILTER_TYPE_SAVED_INSTANCE_STATE = "filter_type";

    private FilterType filterType;

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }

    public void updateSelectedFilters(FilterType selectedFilterType) {
        for(Filter selectedFilter : selectedFilterType.getFilters(getContext())) {
            for(Filter filter : filterType.getFilters(getContext())) {
                if(selectedFilter.getValue().equals(filter.getValue())) {
                    filter.setSelected(true);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            Log.d(TAG, "Loading saved instance state");
            filterType = (FilterType) savedInstanceState.getSerializable(FILTER_TYPE_SAVED_INSTANCE_STATE);
        }
        View root = inflater.inflate(R.layout.fragment_filter, container, false);
        initFilterTitle(root);
        initRecyclerView(root, filterType.getFilters(getContext()));
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        outState.putSerializable(FILTER_TYPE_SAVED_INSTANCE_STATE, filterType);
    }

    private void initFilterTitle(View root) {
        TextView filterTitle = (TextView) root.findViewById(R.id.filter_title);
        filterTitle.setText(filterType.getName());
    }

    private void initRecyclerView(View root, List<Filter> filters) {
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new FilterAdapter(getContext(), filters));
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(30));
    }

    @Override
    public boolean validate() {
        switch (filterType.getType()) {
            //at least one
            case FilterType.FILTER_TYPE_TIME:
            case FilterType.FILTER_TYPE_PRICE:
            case FilterType.FILTER_TYPE_SOCIAL:
                for(Filter filter : filterType.getFilters(getContext())) {
                    if(filter.isSelected())
                        return true;
                }
                Toast.makeText(getContext(), R.string.suggestion_validation_at_least_one, Toast.LENGTH_SHORT).show();
                return false;
            //exactly one
            case FilterType.FILTER_TYPE_PASSION:
                boolean selected = false;
                for(Filter filter : filterType.getFilters(getContext())) {
                    if(filter.isSelected()) {
                        if(selected) {
                            selected = false;
                            break;
                        } else
                            selected = true;
                    }
                }
                if(selected)
                    return true;
                Toast.makeText(getContext(), R.string.suggestion_validation_exactly_one, Toast.LENGTH_SHORT).show();
                return false;
        }
        return false;
    }

    @Override
    public void save(ActivitySuggestion activitySuggestion) {
        activitySuggestion.getTags().add(filterType);
    }
}

package com.passionusnetworks.lype.fragments;

import android.support.v4.app.Fragment;

import com.passionusnetworks.lype.models.ActivitySuggestion;

/**
 * Created by Raphael on 2016-08-29.
 */

public abstract class SuggestionFragment extends Fragment {
    abstract public boolean validate();
    abstract public void save(ActivitySuggestion activitySuggestion);
}

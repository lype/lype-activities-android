package com.passionusnetworks.lype.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.activities.SuggestionActivity;
import com.passionusnetworks.lype.models.ActivitySuggestion;

/**
 * Created by Raphael on 2016-04-18.
 */
public class SuggestionWhatFragment extends SuggestionFragment {
    private static final String TAG = "SuggestionWhatFragment";
    public static final int PICK_IMAGE = 1;
    private static final String TITLE_SAVED_INSTANCE_STATE = "title";
    private static final String DESCRIPTION_SAVED_INSTANCE_STATE = "description";
    private static final String WEBSITE_SAVED_INSTANCE_STATE = "description";
    private static final String IMAGE_SAVED_INSTANCE_STATE = "image";

    private EditText title;
    private EditText description;
    private EditText website;
    private LinearLayout pickImage;
    private RelativeLayout changeImage;
    private ImageView image;
    private Uri selectedImageUri = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_suggestion_what, container, false);
        initLayout(root);

        if(savedInstanceState != null) {
            if(savedInstanceState.getString(TITLE_SAVED_INSTANCE_STATE) != null)
                title.setText(savedInstanceState.getString(TITLE_SAVED_INSTANCE_STATE));
            if(savedInstanceState.getString(DESCRIPTION_SAVED_INSTANCE_STATE) != null)
                description.setText(savedInstanceState.getString(DESCRIPTION_SAVED_INSTANCE_STATE));
            if(savedInstanceState.getParcelable(IMAGE_SAVED_INSTANCE_STATE) != null)
                image.setImageURI((Uri) savedInstanceState.getParcelable(IMAGE_SAVED_INSTANCE_STATE));
        }
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(title != null && !title.getText().toString().isEmpty())
            outState.putString(TITLE_SAVED_INSTANCE_STATE, title.getText().toString());
        if(description != null && !description.getText().toString().isEmpty())
            outState.putString(DESCRIPTION_SAVED_INSTANCE_STATE, description.getText().toString());
        if(website != null && !website.getText().toString().isEmpty())
            outState.putString(WEBSITE_SAVED_INSTANCE_STATE, website.getText().toString());
        if(selectedImageUri != null)
            outState.putParcelable(IMAGE_SAVED_INSTANCE_STATE, selectedImageUri);
    }

    private void initLayout(View root) {
        title = (EditText) root.findViewById(R.id.suggestion_title);
        description = (EditText) root.findViewById(R.id.suggestion_description);
        website = (EditText) root.findViewById(R.id.suggestion_website);
        image = (ImageView) root.findViewById(R.id.suggestion_image);
        pickImage = (LinearLayout) root.findViewById(R.id.suggestion_pick_image);
        changeImage = (RelativeLayout) root.findViewById(R.id.suggestion_change_image);
        View.OnClickListener pickImageClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(intent, PICK_IMAGE);
            }
        };
        pickImage.setOnClickListener(pickImageClickListener);
        changeImage.setOnClickListener(pickImageClickListener);
        load();
    }

    private void load() {
        ActivitySuggestion activitySuggestion = ((SuggestionActivity) getActivity()).getActivitySuggestion();
        if(activitySuggestion != null) {
            if (activitySuggestion.getName() != null)
                title.setText(activitySuggestion.getName());
            if (activitySuggestion.getDescription() != null)
                description.setText(activitySuggestion.getDescription());
            if (activitySuggestion.getWebsite() != null)
                website.setText(activitySuggestion.getWebsite());
            if (activitySuggestion.getImage() != null)
                updateImage(activitySuggestion.getImage());
        }
    }

    public void updateImage(Uri selectedImage){
        this.selectedImageUri = selectedImage;
        image.setImageURI(selectedImage);
        pickImage.setVisibility(View.GONE);
        changeImage.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean validate() {
        if(title.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), R.string.suggestion_validation_title, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(description.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), R.string.suggestion_validation_description, Toast.LENGTH_SHORT).show();
            return false;
        }
        if(selectedImageUri == null) {
            Toast.makeText(getContext(), R.string.suggestion_validation_image, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void save(ActivitySuggestion activitySuggestion) {
        activitySuggestion.setName(title.getText().toString());
        activitySuggestion.setDescription(description.getText().toString());
        if(!website.getText().toString().isEmpty())
            activitySuggestion.setWebsite(website.getText().toString());
        activitySuggestion.setImage(selectedImageUri);
    }
}

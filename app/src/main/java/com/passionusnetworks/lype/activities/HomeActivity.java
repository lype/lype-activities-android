package com.passionusnetworks.lype.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.adapters.CategoryAdapter;
import com.passionusnetworks.lype.itemdecorations.HorizontalSpaceItemDecoration;
import com.passionusnetworks.lype.login.LoginHelper;
import com.passionusnetworks.lype.login.LoginResultListener;
import com.passionusnetworks.lype.models.Category;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.persistence.LypeDatabase;
import com.passionusnetworks.lype.persistence.PropertiesManager;
import com.passionusnetworks.lype.services.http.LypeActivitiesApi;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class HomeActivity extends AppCompatActivity implements LoginResultListener {
    private static final String TAG = "HomeActivity";

    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Spinner whenSpinner;
    private Location location;
    private Menu menu;
    private LoginHelper loginHelper;
    private Handler handler;

    public HomeActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        handler = new Handler();
        PropertiesManager.loadProperties(HomeActivity.this);
        loginHelper = new LoginHelper(this, this);
        loginHelper.silentLogin();
        setContentView(R.layout.activity_home);
        //initWhenSpinner();
        //getLocationAndMoments();
        initLayout();
        getLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.home_menu, menu);
        this.menu = menu;
        updateLoginButtons();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorites:
                Intent favoritesActivityIntent = new Intent(this, FavoritesActivity.class);
                startActivity(favoritesActivityIntent);
                return true;
            case R.id.sign_in:
                Intent loginActivityIntent = new Intent(this, LoginActivity.class);
                startActivity(loginActivityIntent);
                return true;
            case R.id.suggestion:
                Intent suggestionActivityIntent = new Intent(this, SuggestionActivity.class);
                startActivity(suggestionActivityIntent);
                return true;
            case R.id.review:
                Intent activitiesActivityIntent = new Intent(this, ActivitiesActivity.class);
                activitiesActivityIntent.putExtra(ActivitiesActivity.EXTRA_MODE, ActivitiesActivity.MODE_REVIEW);
                startActivity(activitiesActivityIntent);
                return true;
            case R.id.sign_out:
                signOut();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        LypeDatabase.getInstance(HomeActivity.this).clearDatabase();
                    }
                });
                loginHelper.silentLogin();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        updateLoginButtons();
    }

    private void signOut() {
        Log.d(TAG, "Signing out");
        PropertiesManager.clearLoginInformation();
        PropertiesManager.saveProperties();
        updateLoginButtons();
    }

    private void updateLoginButtons() {
        if(menu != null) {
            boolean isSignedIn = PropertiesManager.isSignedIn();
            menu.findItem(R.id.sign_in).setVisible(!isSignedIn);
            menu.findItem(R.id.sign_out).setVisible(isSignedIn);
            menu.findItem(R.id.suggestion).setVisible(isSignedIn);
            menu.findItem(R.id.review).setVisible(isSignedIn && PropertiesManager.isUserModerator());
        }
    }

    private LypeActivitiesApi getApi() {
        return LypeActivitiesApiService.getInstance(this).getApi();
    }

    private LypeActivitiesApi getMockedApi() {
        return LypeActivitiesApiService.getInstance(this).getMockedApi();
    }

    @Override
    public void onLoginSuccessful(String username) {
        Log.d(TAG, "onLoginSuccessful: " + username);
    }

    @Override
    public void onLoginFailed(int stringId) {
        if(stringId > 0) {
            final String message = getString(stringId);
            Log.e(TAG, message);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Log.e(TAG, "onLoginFailed");
        }
    }

    private void initLayout() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.categories);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.addItemDecoration(new HorizontalSpaceItemDecoration(30));
        loadCategories(recyclerView);
        CardView anything = (CardView) findViewById(R.id.anything_card);
        anything.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ActivitiesActivity.class);
                startActivity(intent);
            }
        });
        CardView customize = (CardView) findViewById(R.id.customize_card);
        customize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, FiltersActivity.class);
                startActivity(intent);
            }
        });
    }

    /*private void initWhenSpinner() {
        whenSpinner = (Spinner) findViewById(R.id.when);
        whenSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String when = (String) whenSpinner.getAdapter().getItem(position);
                getMeteoAndCategories(when, location);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/

    private void getLocationAndMoments() {
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        compositeSubscription.add(Observable.zip(
                locationProvider.getLastKnownLocation(),
                getMockedApi().getMoments(),
                new Func2<Location, List<String>, List<String>>() {
                    @Override
                    public List<String> call(Location location, List<String> strings) {
                        Log.d(TAG, "location received: " + location);
                        HomeActivity.this.location = location;
                        return strings;
                    }
                }
                )
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<List<String>>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getLocationAndMoments onCompleted");
                                mockDataIfNeeded();
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "getLocationAndMoments onError");
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(List<String> moments) {
                                whenSpinner.setAdapter(new ArrayAdapter<>(HomeActivity.this, R.layout.spinner_item, moments));
                            }
                        })
        );
    }

    private void getLocation() {
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        compositeSubscription.add(locationProvider.getLastKnownLocation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Location>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getLastKnownLocation onCompleted");
                        mockLocationDataIfNeeded();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "getLastKnownLocation onError");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Location location) {
                        Log.d(TAG, "location received: " + location);
                        HomeActivity.this.location = location;
                    }
                })
        );
    }

    private void mockDataIfNeeded() {
        mockLocationDataIfNeeded();
        if(whenSpinner.getAdapter() == null) {
            Log.w(TAG, "No moment received, mocking them.");
            List<String> moments = new ArrayList<>();
            moments.add("right now");
            moments.add("tonight");
            moments.add("tomorrow");
            moments.add("next weekend");
            moments.add("Enter date...");
            whenSpinner.setAdapter(new ArrayAdapter<>(HomeActivity.this, R.layout.spinner_item, moments));
        }
    }

    private void mockLocationDataIfNeeded() {
        if(location == null) {
            Log.w(TAG, "No location found, mocking it.");
            location = new Location("mock");
            location.setLongitude(-73);
            location.setLatitude(45);
        }
    }

    /*private void getMeteoAndCategories(String when, Location where) {
        getMeteoIcon(when, where);
        createCategoryGrid();
    }

    private void getMeteoIcon(String whenString, Location location) {
        compositeSubscription.add(getMockedApi().getMeteo(whenString, location)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getMeteo completed");
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(String s) {
                                ImageView weather = (ImageView) findViewById(R.id.weather);
                                weather.setImageResource(getResources().getIdentifier(s, "drawable", getPackageName()));
                            }
                        })
        );
    }

    private void createCategoryGrid() {
        compositeSubscription.add(getApi().getCategories()
                //get categories in background
                .subscribeOn(Schedulers.io())
                //adding the 2 last category buttons
                .flatMap(new Func1<List<Category>, Observable<List<Category>>>() {
                    @Override
                    public Observable<List<Category>> call(List<Category> categories) {
                        categories.add(new Category(Category.CATEGORY_ANYTHING, "I could do anything!", null, null));
                        categories.add(new Category(Category.CATEGORY_MORE, "More...", null, null));
                        return Observable.just(categories);
                    }
                })
                //load category images
                .flatMap(new Func1<List<Category>, Observable<List<Category>>>() {
                    @Override
                    public Observable<List<Category>> call(List<Category> categories) {
                        Log.d(TAG, "categories loaded, loading images");
                        for (Category category : categories) {
                            try {
                                if (category.getImageUrl() != null)
                                    category.setImage(scaleBitmap(Picasso.with(HomeActivity.this).load(category.getImageUrl()).get(), 100));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        return Observable.just(categories);
                    }
                })
                //receive categories on main thread
                .observeOn(AndroidSchedulers.mainThread())
                //create the layout with categories
                .subscribe(new Subscriber<List<Category>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getCategories onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<Category> categories) {
                        Log.d(TAG, "images loaded, creating category grid");
                        final LinearLayout categoryGrid = (LinearLayout) findViewById(R.id.categories);
                        categoryGrid.removeAllViews();
                        for (int i = 0; i < 4; i++) {
                            final LinearLayout row = createRow();
                            for (int j = 0; j < 2; j++) {
                                try {
                                    Category category = categories.get(i * 2 + j);
                                    LinearLayout categoryButton = createCategoryButton(category);
                                    row.addView(categoryButton);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            categoryGrid.addView(row);
                        }
                    }
                })
        );
    }*/

    private void loadCategories(final RecyclerView recyclerView) {
        compositeSubscription.add(getMockedApi().getFilters().subscribe(new Subscriber<List<FilterType>>() {
            @Override
            public void onCompleted() {
                Log.d(TAG, "getFilters onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(List<FilterType> filterTypes) {
                Log.d(TAG, "filters loaded, loading categories");
                compositeSubscription.add(getApi().getCategories()
                        //get categories in background
                        .subscribeOn(Schedulers.io())
                        //receive categories on main thread
                        .observeOn(AndroidSchedulers.mainThread())
                        //create the layout with categories
                        .subscribe(new Subscriber<List<Category>>() {
                            @Override
                            public void onCompleted() {
                                Log.d(TAG, "getCategories onCompleted");
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onNext(List<Category> categories) {
                                Log.d(TAG, "images loaded, adding categories to recyclerview");
                                recyclerView.setAdapter(new CategoryAdapter(HomeActivity.this, categories));
                            }
                        })
                );
            }
        }));
    }

    private LinearLayout createRow() {
        LinearLayout row = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0);
        layoutParams.weight = 1;
        row.setLayoutParams(layoutParams);
        row.setOrientation(LinearLayout.HORIZONTAL);
        return row;
    }

    private LinearLayout createCategoryButton(final Category category) throws IOException {
        LinearLayout layout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1;
        layoutParams.setMargins(10, 10, 10, 10);
        layout.setLayoutParams(layoutParams);
        layout.setBackgroundResource(R.drawable.category_button);

        LinearLayout innerLayout = new LinearLayout(this);
        LinearLayout.LayoutParams innerLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        innerLayout.setLayoutParams(innerLayoutParams);
        innerLayout.setOrientation(LinearLayout.VERTICAL);
        innerLayout.setClickable(true);
        innerLayout.setFocusable(true);
        innerLayout.setBackgroundResource(getSelectableItemBackgroundResource());
        innerLayout.setGravity(Gravity.CENTER);
        layout.addView(innerLayout);

        View.OnClickListener onClickListener;
        if(category.getId() == Category.CATEGORY_MORE) {
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, FiltersActivity.class);
                    startActivity(intent);
                }
            };
        } else {
            if (category.getImage() != null) {
                ImageView imageView = createCategoryImageView(category.getImage());
                innerLayout.addView(imageView);
            }
            onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, ActivitiesActivity.class);
                    intent.putExtra("filters", (Serializable) category.getFilters());
                    //intent.putExtra("category_id", category.getServerId());
                    //startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(HomeActivity.this).toBundle());
                    startActivity(intent);
                }
            };
        }
        innerLayout.setOnClickListener(onClickListener);

        TextView textView = createCategoryTextView(category.getName());
        innerLayout.addView(textView);

        return layout;
    }

    private ImageView createCategoryImageView(Drawable image) throws IOException {
        ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imageView.setLayoutParams(imageLayoutParams);
        imageView.setImageDrawable(image);
        image.setColorFilter(getResources().getColor(R.color.imageColor), PorterDuff.Mode.SRC_ATOP);
        return imageView;
    }

    private TextView createCategoryTextView(String text) {
        TextView textView = new TextView(this);
        LinearLayout.LayoutParams textLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(textLayoutParams);
        textView.setText(text);
        textView.setTextColor(getResources().getColor(R.color.textColor));
        return textView;
    }

    private int getSelectableItemBackgroundResource() {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = this.obtainStyledAttributes(attrs);
        int backgroundResource = typedArray.getResourceId(0, 0);
        typedArray.recycle();
        return backgroundResource;
    }

    public static Drawable scaleBitmap(Bitmap bitmap, float newHeight) {
        float ratio = newHeight / bitmap.getHeight();
        return new BitmapDrawable(Resources.getSystem(), Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth() * ratio), (int)(bitmap.getHeight() * ratio), true));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeSubscription.unsubscribe();
    }
}

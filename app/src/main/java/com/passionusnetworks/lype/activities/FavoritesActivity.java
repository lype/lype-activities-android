package com.passionusnetworks.lype.activities;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.adapters.FavoriteAdapter;
import com.passionusnetworks.lype.itemdecorations.VerticalSpaceItemDecoration;
import com.passionusnetworks.lype.listeners.FavoritesListListener;
import com.passionusnetworks.lype.models.LypeActivity;
import com.passionusnetworks.lype.persistence.PropertiesManager;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2016-04-30.
 */
public class FavoritesActivity extends AppCompatActivity implements FavoritesListListener {
    private static final String TAG = "FavoritesActivity";

    private List<LypeActivity> favorites = new ArrayList<>();
    private FavoriteAdapter adapter;
    private int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        initToolbar();
        loadFavorites();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.favorites_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.favorites_title));
    }

    private void loadFavorites() {
        if(PropertiesManager.isSignedIn()) {
            Log.d(TAG, "Loading favorites from server");
            LypeActivitiesApiService.getInstance(this).getApi().getFavorites(offset)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<List<LypeActivity>>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "getFavorites onCompleted");
                            if(adapter == null)
                                initRecyclerView();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            loadFavoritesFromDatabase();
                        }

                        @Override
                        public void onNext(final List<LypeActivity> loadedFavorites) {
                            favorites.addAll(loadedFavorites);
                            AsyncTask.execute(new Runnable() {
                                @Override
                                public void run() {
                                    for(LypeActivity favorite : loadedFavorites) {
                                        if(LypeActivity.findFromServerId(FavoritesActivity.this, favorite.getServerId()) == null)
                                            favorite.save(FavoritesActivity.this);
                                    }
                                }
                            });
                            offset++;
                            if(adapter != null)
                                adapter.addFavorites(loadedFavorites);
                        }
                    });
        } else {
            loadFavoritesFromDatabase();
        }
    }

    @Override
    public void onLastItemReached() {
        if(PropertiesManager.isSignedIn()) {
            //we want to load more favorites only from server (because all favorites will be loaded at once in local)
            loadFavorites();
        }
    }

    private void loadFavoritesFromDatabase() {
        Log.d(TAG, "Loading favorites from database");
        favorites = LypeActivity.findAll(this);
        initRecyclerView();
    }

    private void initRecyclerView() {
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.favorites);
        adapter = new FavoriteAdapter(this, favorites, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(50));
        ItemTouchHelper swipeToDelete = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final LypeActivity lypeActivity = adapter.removeItem(viewHolder.getAdapterPosition());
                Snackbar.make(recyclerView, R.string.favorites_deleted, Snackbar.LENGTH_LONG).setAction(R.string.favorites_undo, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        adapter.undoRemove();
                    }
                }).setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        if(event != Snackbar.Callback.DISMISS_EVENT_ACTION) {
                            Log.d(TAG, "onDismissed");
                            removeFavoriteFromDatabase(lypeActivity);
                            sendActivityFeedback(lypeActivity);
                        }
                    }
                }).setActionTextColor(Color.YELLOW).show();
            }
        });
        swipeToDelete.attachToRecyclerView(recyclerView);
    }

    private void removeFavoriteFromDatabase(LypeActivity lypeActivity) {
        if (lypeActivity.getId() == 0) {
            Log.d(TAG, "Loading activity from database");
            lypeActivity = LypeActivity.findFromServerId(this, lypeActivity.getServerId());
            if(lypeActivity != null) {
                Log.d(TAG, "Activity loaded from database");
            } else {
                Log.e(TAG, "Failed to load activity from database");
            }
        }
        if (lypeActivity != null && lypeActivity.getId() != 0) {
            Log.i(TAG, "Deleting activity from database");
            lypeActivity.delete(this);
        } else {
            Log.w(TAG, "Could not delete activity from database");
        }
    }

    private void sendActivityFeedback(LypeActivity lypeActivity) {
        try {
            LypeActivitiesApiService.getInstance(this).getApi()
                    .putActivityFeedback(lypeActivity.getServerId(), false)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "putActivityFeedback onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(Boolean b) {
                            Log.d(TAG, "putActivityFeedback onNext: " + b);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

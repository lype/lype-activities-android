package com.passionusnetworks.lype.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.fragments.FilterFragment;
import com.passionusnetworks.lype.fragments.SuggestionFragment;
import com.passionusnetworks.lype.fragments.SuggestionWhatFragment;
import com.passionusnetworks.lype.fragments.SuggestionWhereFragment;
import com.passionusnetworks.lype.models.ActivitySuggestion;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2016-08-25.
 */
public class SuggestionActivity extends AppCompatActivity {
    private static final String TAG = "SuggestionActivity";

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private List<FilterType> filterTypes;
    private TextView message;
    private ProgressBar progressBar;
    private ArrayList<SuggestionFragment> fragments = new ArrayList<>();
    private int currentFragment = -1;
    private Menu menu;
    private ActivitySuggestion activitySuggestion;
    private boolean uploadSuccessful = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);
        activitySuggestion = new ActivitySuggestion();  //TODO create from saved instance
        initToolbar();
        loadFilters();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SuggestionWhatFragment.PICK_IMAGE) {
            if(resultCode != RESULT_OK) {
                Log.e(TAG, "Result code is not OK, " + resultCode);
                return;
            }
            if(data == null) {
                Log.e(TAG, "No image picked");
                return;
            }
            Uri selectedImage = data.getData();
            ((SuggestionWhatFragment) fragments.get(currentFragment)).updateImage(selectedImage);
        } else if (requestCode == SuggestionWhereFragment.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i(TAG, "Place: " + place.getName());
                ((SuggestionWhereFragment) fragments.get(currentFragment)).updateAddress(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else {
            Log.e(TAG, "Unknown request code " + requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                Log.d(TAG, "onRequestPermissionsResult for storage permission");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "permission granted");
                    // permission was granted
                    sendSuggestion();
                } else {
                    Log.w(TAG, "permission denied");
                    // permission denied
                    Toast.makeText(SuggestionActivity.this, getString(R.string.suggestion_error_permission), Toast.LENGTH_LONG).show();
                    enableActionButton();
                }
            }
            default:
                enableActionButton();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.suggestion_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.suggestion_action:
                if(fragments.get(currentFragment).validate()) {
                    fragments.get(currentFragment).save(activitySuggestion);
                    next();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initLayout() {
        progressBar = (ProgressBar) findViewById(R.id.suggestion_progress);
        progressBar.setMax(6);
        int color = getResources().getColor(R.color.colorPrimary);
        progressBar.getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        progressBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        message = (TextView) findViewById(R.id.suggestion_message);
        next();
    }

    private void next() {
        SuggestionFragment fragment = null;
        if(fragments.size() <= currentFragment + 1) {
            //we need to create
            switch (currentFragment) {
                case -1:
                    fragment = new SuggestionWhatFragment();
                    break;
                case 0:
                    fragment = new SuggestionWhereFragment();
                    break;
                case 1:
                    fragment = new FilterFragment();
                    ((FilterFragment) fragment).setFilterType(filterTypes.get(0));
                    break;
                case 2:
                    fragment = new FilterFragment();
                    ((FilterFragment) fragment).setFilterType(filterTypes.get(1));
                    break;
                case 3:
                    fragment = new FilterFragment();
                    ((FilterFragment) fragment).setFilterType(filterTypes.get(2));
                    break;
                case 4:
                    fragment = new FilterFragment();
                    ((FilterFragment) fragment).setFilterType(filterTypes.get(4));
                    break;
            }
            if(fragment != null) {
                fragments.add(fragment);
                ++currentFragment;
            } else {
                menu.findItem(R.id.suggestion_action).setEnabled(false);
                //check for storage permissions before sending suggestion
                verifyStoragePermissions();
                return;
            }
        } else {
            //fragment already created
            fragment = fragments.get(++currentFragment);
        }
        update();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.suggestion_frame, fragment)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    private void sendSuggestion() {
        try {
            Toast.makeText(this, getString(R.string.suggestion_sending_suggestion), Toast.LENGTH_SHORT).show();
            LypeActivitiesApiService.getInstance(this).getApi().postSuggestion(activitySuggestion)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "postSuggestion onCompleted");
                            if (activitySuggestion.getId() != null)
                                uploadImage();
                            else
                                errorSendingSuggestion();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            errorSendingSuggestion();
                        }

                        @Override
                        public void onNext(String id) {
                            Log.d(TAG, "onNext: " + id);
                            activitySuggestion.setId(id);
                        }
                    });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    private void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission is not granted, asking for it");
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                createDismissableDialog(getString(R.string.suggestion_permission_explanation), new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        requestReadExternalStoragePermission();
                    }
                });
            } else {
                requestReadExternalStoragePermission();
            }
        } else {
            Log.d(TAG, "Permission already granted");
            sendSuggestion();
        }
    }

    private void requestReadExternalStoragePermission() {
        // We don't have permission so prompt the user
        ActivityCompat.requestPermissions(SuggestionActivity.this, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
    }

    private void createDismissableDialog(String text, DialogInterface.OnDismissListener onDismissListener) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.tutorial_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnDismissListener(onDismissListener);
        TextView dialogText = (TextView) dialog.findViewById(R.id.dialog_text);
        dialogText.setText(text);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.got_it);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void errorSendingSuggestion() {
        Toast.makeText(SuggestionActivity.this, getString(R.string.suggestion_error_sending_suggestion), Toast.LENGTH_LONG).show();
        enableActionButton();
    }

    private void uploadImage() {
        Toast.makeText(SuggestionActivity.this, getString(R.string.suggestion_uploading_image), Toast.LENGTH_SHORT).show();
        LypeActivitiesApiService.getInstance(this).getApi().putSuggestionImage(activitySuggestion)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "putSuggestionImage onCompleted");
                        if(uploadSuccessful) {
                            Toast.makeText(SuggestionActivity.this, getString(R.string.suggestion_success), Toast.LENGTH_LONG).show();
                            finish();
                        } else
                            errorUploadingImage();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        errorUploadingImage();
                    }

                    @Override
                    public void onNext(String jsonResponse) {
                        Log.d(TAG, "onNext: " + jsonResponse);
                        uploadSuccessful = jsonResponse != null;
                    }
                });
    }

    private void errorUploadingImage() {
        Toast.makeText(SuggestionActivity.this, getString(R.string.suggestion_error_uploading_image), Toast.LENGTH_LONG).show();
        enableActionButton();
    }

    private void enableActionButton() {
        menu.findItem(R.id.suggestion_action).setEnabled(true);
    }

    public ActivitySuggestion getActivitySuggestion() {
        return activitySuggestion;
    }

    private void update() {
        updateMessage();
        updateMenuAction();
        updateProgress();
    }

    private void updateMessage() {
        int messageId = 0;
        switch (currentFragment) {
            case 0:
                messageId = R.string.suggestion_what;
                break;
            case 1:
                messageId = R.string.suggestion_where;
                break;
            case 2:
                messageId = R.string.suggestion_when;
                break;
            case 3:
                messageId = R.string.suggestion_how_much;
                break;
            case 4:
                messageId = R.string.suggestion_with_whom;
                break;
            case 5:
                messageId = R.string.suggestion_what_kind;
                break;
        }
        if(messageId != 0)
            message.setText(messageId);
    }

    private void updateMenuAction() {
        if(menu != null) {
            MenuItem action = menu.findItem(R.id.suggestion_action);
            action.setTitle(currentFragment == 5 ? R.string.menu_send : R.string.menu_next);
        }
    }

    private void updateProgress() {
        progressBar.setProgress(currentFragment + 1);
    }

    private void initToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.suggestion_title));
    }

    private void loadFilters() {
        LypeActivitiesApiService.getInstance(this).getMockedApi().getFilters()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<FilterType>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getFilters onCompleted");
                        initLayout();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<FilterType> loadedFilterTypes) {
                        filterTypes = loadedFilterTypes;
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {
        showConfirmationPopup();
        return true;
    }

    @Override
    public void onBackPressed() {
        if(currentFragment == 0) {
            showConfirmationPopup();
            return;
        }
        --currentFragment;
        update();
        super.onBackPressed();
    }

    private void showConfirmationPopup() {
        AlertDialog popup = new AlertDialog.Builder(this)
                .setMessage(R.string.suggestion_confirm_exit)
                .setNegativeButton(R.string.suggestion_confirm_exit_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.suggestion_confirm_exit_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create();
        popup.show();
    }
}

package com.passionusnetworks.lype.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.models.Comment;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONException;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2016-12-11.
 */

public class CommentActivity extends AppCompatActivity {
    private static final String TAG = "CommentActivity";

    private String activityId;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        activityId = getIntent().getStringExtra("activity_id");
        setContentView(R.layout.activity_comment);
        initToolbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.comment_menu, menu);
        this.menu = menu;
        return true;
    }

    private void initToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.comment_write));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.comment_post:
                postComment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void postComment() {
        try {
            menu.findItem(R.id.comment_post).setEnabled(false);
            String title = ((EditText) findViewById(R.id.comment_title)).getText().toString();
            String text = ((EditText) findViewById(R.id.comment_content)).getText().toString();
            LypeActivitiesApiService.getInstance(this).getApi()
                    .postComment(activityId, title, text)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Comment>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "postComment onCompleted");
                            finish();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            menu.findItem(R.id.comment_post).setEnabled(true);
                            Toast.makeText(getBaseContext(), R.string.comment_error, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNext(Comment comment) {
                            Log.d(TAG, "postComment onNext: " + comment.toString());
                            Intent commentIntent = new Intent();
                            commentIntent.putExtra("comment", comment);
                            setResult(RESULT_OK, commentIntent);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            menu.findItem(R.id.comment_post).setEnabled(true);
        }
    }
}

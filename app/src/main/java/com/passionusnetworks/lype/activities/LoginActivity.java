package com.passionusnetworks.lype.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.login.LoginHelper;
import com.passionusnetworks.lype.login.LoginResultListener;
import com.passionusnetworks.lype.persistence.PropertiesManager;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONObject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael Royer-Rivard on 2015-07-25.
 */
public class LoginActivity extends FragmentActivity implements LoginResultListener {
    private static final String TAG = "LoginActivity";

    private Handler handler;
    private LoginHelper loginHelper;

    @Override
    public void onCreate(Bundle args) {
        Log.d(TAG, "onCreate");
        super.onCreate(args);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        handler = new Handler();
        loginHelper = new LoginHelper(this, this);
        TextView signInWithEmail = (TextView) findViewById(R.id.sign_in_with_email);
        signInWithEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.third_party_layout).setVisibility(View.GONE);
                findViewById(R.id.email_layout).setVisibility(View.VISIBLE);
            }
        });
        final TextView email = (TextView) findViewById(R.id.email);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView signIn = (TextView) findViewById(R.id.sign_in);
        password.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    signIn.performClick();
                }
                return false;
            }
        });
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userEmail = email.getText().toString();
                String userPassword = password.getText().toString();
                PropertiesManager.setEmail(userEmail);
                PropertiesManager.setPassword(userPassword);
                PropertiesManager.saveProperties();

                loginHelper.loginWithEmail();
            }
        });
        //Google sign in
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginHelper.loginWithGoogle();
            }
        });
        //Facebook sign in
        LoginButton facebookLoginButton = (LoginButton) findViewById(R.id.login_button);
        facebookLoginButton.setReadPermissions("public_profile, email");
        facebookLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginHelper.loginWithFacebook();
            }
        });
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent createAccountIntent = new Intent(LoginActivity.this, CreateAccountActivity.class);
                startActivity(createAccountIntent);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginHelper.manageActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        View emailLayout = findViewById(R.id.email_layout);
        if(emailLayout.getVisibility() == View.VISIBLE) {
            emailLayout.setVisibility(View.GONE);
            findViewById(R.id.third_party_layout).setVisibility(View.VISIBLE);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onLoginSuccessful(final String username) {
        Log.d(TAG, "onLoginSuccessful");
        handler.post(new Runnable() {
            @Override
            public void run() {
                String text = username != null ? String.format(getString(R.string.login_success), username) : getString(R.string.login_success_without_name);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
        finish();
    }

    @Override
    public void onLoginFailed(int stringId) {
        if(stringId > 0) {
            final String message = getString(stringId);
            Log.e(TAG, message);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Log.e(TAG, "onLoginFailed");
        }
    }
}

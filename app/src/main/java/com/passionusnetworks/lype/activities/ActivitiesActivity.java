package com.passionusnetworks.lype.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.google.android.gms.location.LocationRequest;
import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.adapters.CommentAdapter;
import com.passionusnetworks.lype.behaviors.SmoothScrollBehavior;
import com.passionusnetworks.lype.listeners.OnHorizontalSwipeTouchListener;
import com.passionusnetworks.lype.models.Comment;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.models.LypeActivity;
import com.passionusnetworks.lype.persistence.PropertiesManager;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;
import com.passionusnetworks.lype.views.LockableNestedScrollView;
import com.passionusnetworks.lype.views.MovingRelativeLayout;
import com.passionusnetworks.lype.views.TagView;
import com.squareup.picasso.Picasso;

import org.apmem.tools.layouts.FlowLayout;
import org.json.JSONException;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2016-03-17.
 */
public class ActivitiesActivity extends AppCompatActivity {
    private static final String TAG = "ActivitiesActivity";

    // Storage Permissions
    private static final int REQUEST_ACCESS_COARSE_LOCATION = 1;
    private static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private static final int REQUEST_CODE_POST_COMMENT = 1;

    public static final String EXTRA_FAVORITE = "favorite";
    public static final String EXTRA_MODE = "mode";
    public static final String MODE_REVIEW = "review";
    public static final String MODE_FAVORITE = "favorite";

    private final int DEMO_TIMEOUT = 750;

    private Location location;
    private List<FilterType> filters;
    private List<LypeActivity> activities = new ArrayList<>();
    private int currentActivity = 0;
    private boolean stopSpring = false;
    private float leftContentWidth;
    private float rightContentWidth;
    private String mode;
    private CommentAdapter commentAdapter;
    private LockableNestedScrollView lockableNestedScrollView;
    private SmoothScrollBehavior smoothScrollBehavior;
    private boolean preventClick = false;
    private int pageOffset = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        setTheme(R.style.NoActionBarTheme);
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(android.view.Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.activity_activities);
        mode = getIntent().getStringExtra(EXTRA_MODE);
        initToolbar();
        initLayout();
        initBackground();
        filters = (List<FilterType>)getIntent().getSerializableExtra("filters");
        if(mode != null) {
            if(mode.equals(MODE_REVIEW))
                loadSuggestions();
            else if(mode.equals(MODE_FAVORITE))
                loadFavorite();
        } else
            loadLocationAndActivities();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent");
        super.onNewIntent(intent);
        activities.clear();
        currentActivity = 0;
        pageOffset = 0;
        filters = (List<FilterType>)intent.getSerializableExtra("filters");
        loadLocationAndActivities();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu");
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activities_menu, menu);
        if(mode != null) {
            MenuItem favorites = menu.findItem(R.id.favorites);
            if(favorites != null)
                favorites.setVisible(false);
            MenuItem filters = menu.findItem(R.id.filters);
            if(filters != null)
                filters.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorites:
                Intent favoritesActivityIntent = new Intent(this, FavoritesActivity.class);
                startActivity(favoritesActivityIntent);
                return true;
            case R.id.filters:
                Intent filtersActivityIntent = new Intent(this, FiltersActivity.class);
                filtersActivityIntent.putExtra("filters", (Serializable) filters);
                startActivity(filtersActivityIntent);
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp_shadowed);
        getSupportActionBar().setTitle("");
    }

    private void initLayout() {
        findViewById(R.id.web_site_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!preventClick) {
                    String url = ((TextView) findViewById(R.id.web_site)).getText().toString();
                    if(URLUtil.isValidUrl(url)) {
                        Intent openUrl = new Intent(Intent.ACTION_VIEW);
                        openUrl.setData(Uri.parse(url));
                        startActivity(openUrl);
                    } else {
                        Log.w(TAG, "Invalid URL: " + url);
                    }
                }
            }
        });

        // Show/hide the floating action button
        final NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.scroll_view);
        final View commentsView = scrollView.findViewById(R.id.comments);
        final View commentsCountView = scrollView.findViewById(R.id.comments_count);
        final FloatingActionButton writeCommentFAB = (FloatingActionButton)findViewById(R.id.write_comment_fab);
        final Rect scrollBounds = new Rect();
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (commentsCountView != null && commentsView != null) {
                    scrollView.getHitRect(scrollBounds);
                    if(commentsCountView.getLocalVisibleRect(scrollBounds) || commentsView.getLocalVisibleRect(scrollBounds)) {
                        if(PropertiesManager.isSignedIn())
                            writeCommentFAB.show();
                    } else
                        writeCommentFAB.hide();
                } else {
                    writeCommentFAB.hide();
                }
            }
        });
        writeCommentFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivitiesActivity.this, CommentActivity.class);
                intent.putExtra("activity_id", activities.get(currentActivity).getServerId());
                startActivityForResult(intent, REQUEST_CODE_POST_COMMENT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_POST_COMMENT && resultCode == RESULT_OK) {
            Comment comment = (Comment) data.getSerializableExtra("comment");
            commentAdapter.addComment(comment);
            updateCommentsCount();
        }
    }

    private void initBackground() {
        View shadow = findViewById(R.id.shadow);
        shadow.setBackgroundColor(Color.BLACK);
        shadow.setAlpha(0);

        View backgroundLeft = findViewById(R.id.background_left);
        View backgroundRight = findViewById(R.id.background_right);
        backgroundLeft.setBackgroundColor(getResources().getColor(R.color.approve));
        backgroundRight.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        ImageView thumbUp = (ImageView) findViewById(R.id.thumb_up);
        ImageView thumbDown = (ImageView) findViewById(R.id.thumb_down);
        thumbUp.setImageResource(R.drawable.ic_thumb_up_black_36dp);
        thumbDown.setImageResource(R.drawable.ic_thumb_down_black_36dp);
        thumbUp.setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        thumbDown.setColorFilter(getResources().getColor(R.color.imageColor), PorterDuff.Mode.SRC_ATOP);

        updateBackgroundTexts();

        observeRootTranslationX();
    }

    private void updateBackgroundTexts() {
        int positiveText;
        int negativeText;
        if(mode != null && mode.equals(MODE_REVIEW)) {
            positiveText = R.string.activity_approve;
            negativeText = R.string.activity_refuse;
        } else {
            positiveText = R.string.activity_interested;
            negativeText = R.string.activity_maybe_later;
        }
        ((TextView) findViewById(R.id.positive_text)).setText(getString(positiveText));
        ((TextView) findViewById(R.id.negative_text)).setText(getString(negativeText));
    }

    private void observeRootTranslationX() {
        final MovingRelativeLayout root = (MovingRelativeLayout) findViewById(R.id.root);
        final View shadow = findViewById(R.id.shadow);
        final View backgroundLeft = findViewById(R.id.background_left);
        final View backgroundRight = findViewById(R.id.background_right);
        final View leftContent = findViewById(R.id.background_left_content);
        leftContent.post(new Runnable() {
            @Override
            public void run() {
                leftContentWidth = leftContent.getWidth();
                leftContent.setPivotX(0);
                leftContent.setPivotY(leftContent.getHeight()/2);
            }
        });
        final View rightContent = findViewById(R.id.background_right_content);
        rightContent.post(new Runnable() {
            @Override
            public void run() {
                rightContentWidth = rightContent.getWidth();
                rightContent.setPivotX(rightContent.getWidth());
                rightContent.setPivotY(rightContent.getHeight()/2);
            }
        });
        final View leftDropShadow = findViewById(R.id.left_drop_shadow);
        final View rightDropShadow = findViewById(R.id.right_drop_shadow);
        float screenWidth = getScreenWidth(this);
        final float minimumSwipeEndDistance = OnHorizontalSwipeTouchListener.MINUMUM_SWIPE_END_SCREEN_PERCENTAGE_DISTANCE * screenWidth;
        OnHorizontalSwipeTouchListener.setMinimumSwipeEndDistance(minimumSwipeEndDistance);
        Observer translationXObserver = new Observer() {
            @SuppressWarnings("Range")
            @Override
            public void update(Observable observable, Object data) {
                float translationX = (float) data;
                float ratio = Math.abs(translationX) / minimumSwipeEndDistance;
                //The ratio is 1 or between 0 and 0.5 to give information to the user about when he can let go
                float alpha = ratio >= 1 ? 1 : ratio / 2;
                shadow.setAlpha((1-ratio)/2);
                if(translationX >= 0) {
                    backgroundRight.setVisibility(View.GONE);
                    backgroundLeft.setVisibility(View.VISIBLE);
                    leftContent.setAlpha(alpha);
                    float scale = (translationX - 60) / leftContentWidth;
                    leftContent.setScaleX(scale);
                    leftContent.setScaleY(scale);
                } else {
                    backgroundLeft.setVisibility(View.GONE);
                    backgroundRight.setVisibility(View.VISIBLE);
                    rightContent.setAlpha(alpha);
                    float scale = (-translationX - 20) / rightContentWidth;
                    rightContent.setScaleX(scale);
                    rightContent.setScaleY(scale);
                }
                leftDropShadow.setTranslationX((float) data - leftDropShadow.getWidth());
                rightDropShadow.setTranslationX((float) data + root.getWidth());
            }
        };
        root.addObserver(translationXObserver);
    }

    public static float getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Log.d(TAG, "screen width: " + size.x);
        return size.x;
    }

    private void loadFavorite() {
        String lypeActivityServerId = getIntent().getStringExtra(EXTRA_FAVORITE);
        Log.d(TAG, "loadFavorite with server_id " + lypeActivityServerId);
        LypeActivity lypeActivity = LypeActivity.findFromServerId(this, lypeActivityServerId);
        if(lypeActivity == null)
            Log.e(TAG, "Unable to find activity in database");
        activities = new ArrayList<>();
        activities.add(lypeActivity);
        updateWithActivity(lypeActivity);
        initSmoothScroll();
    }

    private void initFeedbackDemo() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View root = findViewById(R.id.root);
                float initialX = OnHorizontalSwipeTouchListener.getMinimumSwipeEndDistance();
                initDemoAction1(root, initialX);
            }
        }, DEMO_TIMEOUT);
    }

    private void createDismissableDialog(String text, DialogInterface.OnDismissListener onDismissListener) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.tutorial_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnDismissListener(onDismissListener);
        TextView dialogText = (TextView) dialog.findViewById(R.id.dialog_text);
        dialogText.setText(text);
        TextView dialogButton = (TextView) dialog.findViewById(R.id.got_it);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Slide right
     */
    private void initDemoAction1(final View root, final float initialX) {
        createSpring(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                float value = 1 - (float) spring.getCurrentValue();
                float x = initialX * value;
                root.setTranslationX(x);

                if(value == 1) {
                    createDismissableDialog(getString(R.string.activity_tutorial_save), new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            initDemoAction2(root, initialX);
                        }
                    });
                }
            }
        });
    }

    /**
     * Slide left
     */
    private void initDemoAction2(final View root, final float initialX) {
        createSpring(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                float value = (float) spring.getCurrentValue();
                float x = initialX * value * 2 - initialX;
                root.setTranslationX(x);

                if(value == 0) {
                    createDismissableDialog(getString(R.string.activity_tutorial_skip), new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            initDemoAction3(root, initialX);
                        }
                    });
                }
            }
        });
    }

    /**
     * Slide back at original position
     */
    private void initDemoAction3(final View root, final float initialX) {
        createSpring(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                float value = (float) spring.getCurrentValue();
                float x = -initialX * value;
                root.setTranslationX(x);

                if(value == 1) {
                    onDemoFinished();
                }
            }
        });
    }

    private void onDemoFinished() {
        initSwipeListener();
        updateNewUserProperty();
    }

    private void updateNewUserProperty() {
        PropertiesManager.setNewUser(false);
        PropertiesManager.saveProperties();
    }

    private void initSwipeListener() {
        final MovingRelativeLayout root = (MovingRelativeLayout) findViewById(R.id.root);
        OnHorizontalSwipeTouchListener horizontalSwipeTouchListener = new OnHorizontalSwipeTouchListener() {
            @Override
            protected void swiping(float distance) {
                root.setTranslationX(distance);
                stopSpring = true;
            }

            @Override
            protected void onSwipe(int direction) {
                boolean interested = direction == Gravity.RIGHT;
                Log.d(TAG, "Swipe " + (interested ? "right" : "left"));
                blockScrolling(true);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    showRefreshView(interested);
                } else {
                    saveChoice(interested);
                    scrollToTop();
                }
                recenterViewWithSpringAnimation();
            }

            @Override
            protected void onSwipeCanceled() {
                Log.d(TAG, "Swipe canceled");
                recenterViewWithSpringAnimation();
            }
        };
        root.setOnTouchListener(horizontalSwipeTouchListener);
        lockableNestedScrollView = (LockableNestedScrollView) findViewById(R.id.scroll_view);
        lockableNestedScrollView.setOnTouchListener(horizontalSwipeTouchListener);
        CardView webSiteCard = (CardView) findViewById(R.id.web_site_card);
        webSiteCard.setOnTouchListener(horizontalSwipeTouchListener);
        ImageView map = (ImageView) findViewById(R.id.map);
        map.setOnTouchListener(horizontalSwipeTouchListener);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.comments);
        recyclerView.setOnTouchListener(horizontalSwipeTouchListener);

        initSmoothScroll();
    }

    private void initSmoothScroll() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.comments);
        recyclerView.setNestedScrollingEnabled(false);  //otherwise the fling won't work

        AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.app_bar);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)appBarLayout.getLayoutParams();
        smoothScrollBehavior = new SmoothScrollBehavior(R.id.scroll_view);
        params.setBehavior(smoothScrollBehavior);
    }

    private void blockScrolling(boolean block) {
        lockableNestedScrollView.setPreventScroll(block);
        if(smoothScrollBehavior != null)
            smoothScrollBehavior.blockScroll(block);
        else
            Log.e(TAG, "SmoothScrollBehavior is null, cannot " + (block ? "block" : "unblock") + " scrolling.");
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showRefreshView(final boolean approve) {
        final View refreshView = findViewById(R.id.refresh_view);
        int color = getResources().getColor(approve ? R.color.approve : R.color.colorAccent);
        refreshView.setBackgroundColor(color);
        int x = approve ? 0 : refreshView.getWidth();
        int y = refreshView.getHeight()/2;
        float radius = (float) Math.hypot(refreshView.getWidth(), y);
        Animator anim = ViewAnimationUtils.createCircularReveal(refreshView, x, y, 0, radius);
        refreshView.setVisibility(View.VISIBLE);
        anim.start();
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                saveChoice(approve);
                scrollToTop();
                if (currentActivity <= activities.size())
                    hideRefreshView(approve);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void hideRefreshView(boolean approve) {
        if(approve && mode == null) {
            hideRefreshViewWithCircularReveal();
        } else {
            hideRefreshViewWithFadeOut();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void hideRefreshViewWithCircularReveal() {
        final View refreshView = findViewById(R.id.refresh_view);
        View savedForLaterMenuItem = findViewById(R.id.favorites);
        int[] location = new int[2];
        savedForLaterMenuItem.getLocationInWindow(location);
        int x = location[0] + savedForLaterMenuItem.getWidth() / 2;
        int y = location[1] + savedForLaterMenuItem.getHeight() / 2;
        float radius = (float) Math.hypot(x, refreshView.getHeight() - y);
        Animator anim = ViewAnimationUtils.createCircularReveal(refreshView, x, y, radius, 0);
        anim.start();
        setSavedForLaterIconColor(R.color.approve, false, false);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                refreshView.setVisibility(View.INVISIBLE);
                setSavedForLaterIconColor(R.color.textColor, true, true);
            }
        });
    }

    private void setSavedForLaterIconColor(int color, final boolean animated, final boolean reset) {
        ActionMenuItemView savedForLaterMenuItem = (ActionMenuItemView) findViewById(R.id.favorites);
        final Drawable icon = savedForLaterMenuItem.getItemData().getIcon();
        if(animated) {
            ArgbEvaluator evaluator = new ArgbEvaluator();
            ValueAnimator animator = new ValueAnimator();
            animator.setIntValues(getResources().getColor(R.color.approve), getResources().getColor(color));
            animator.setEvaluator(evaluator);
            animator.setDuration(500);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int color = (int) animation.getAnimatedValue();
                    icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
                    if(reset && animation.getCurrentPlayTime() >= animation.getDuration()) {
                        icon.clearColorFilter();
                    }
                }
            });
            animator.start();
        } else {
            icon.setColorFilter(getResources().getColor(color), PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void hideRefreshViewWithFadeOut() {
        final View refreshView = findViewById(R.id.refresh_view);
        Animation anim = new AlphaAnimation(1, 0);
        anim.setDuration(500);
        refreshView.setAnimation(anim);
        anim.start();
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                refreshView.setVisibility(View.INVISIBLE);
            }
            public void onAnimationStart(Animation animation) {}
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    private void loadSuggestions() {
        LypeActivitiesApiService.getInstance(this).getApi().getSuggestions(pageOffset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<LypeActivity>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getSuggestions onCompleted");
                        if(activities.size() > 0) {
                            updateWithActivity(activities.get(currentActivity));
                            onActivitiesLoaded();
                        } else {
                            setNoActivityLayoutVisibility(true);
                            updatePicture("https://ih0.redbubble.net/image.53589210.4056/flat,800x800,075,t.u1.jpg");
                            updateName(getString(R.string.activity_no_suggestion));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<LypeActivity> activities) {
                        ActivitiesActivity.this.activities.addAll(activities);
                        //the offset for suggestions works differently because suggestions get cleared while swiping them
                        pageOffset = 1; //this is an offset of 1 (not 1 page)
                    }
                });
    }

    private void loadLocationAndActivities() {
        final ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        locationProvider.getLastKnownLocation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Location>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getLastKnownLocation completed");
                        if(location == null) {
                            Log.d(TAG, "location is null, requesting coarse location");
                            verifyLocationPermissions();
                        } else
                            loadActivities();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Location location) {
                        ActivitiesActivity.this.location = location;
                    }
                });
    }

    private void loadCoarseLocationAndActivities() {
        Log.d(TAG, "loadCoarseLocationAndActivities");
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setNumUpdates(1);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(1);
        final ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        locationProvider.getUpdatedLocation(locationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        Log.d(TAG, "location: " + location);
                        ActivitiesActivity.this.location = location;
                        loadActivities();
                    }
                });
    }

    /**
     * Checks if the app has permission to write to location the device
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    private void verifyLocationPermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permission is not granted, asking for it");
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                createDismissableDialog(getString(R.string.activity_permission_explanation), new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        requestAccessCoarseLocation();
                    }
                });
            } else {
                requestAccessCoarseLocation();
            }
        } else {
            Log.d(TAG, "Permission already granted");
            loadCoarseLocationAndActivities();
        }
    }

    private void requestAccessCoarseLocation() {
        // We don't have permission so prompt the user
        ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_ACCESS_COARSE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_COARSE_LOCATION:
                Log.d(TAG, "onRequestPermissionsResult for storage permission");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "permission granted");
                    // permission was granted
                    loadCoarseLocationAndActivities();
                } else {
                    Log.w(TAG, "permission denied");
                    // permission denied
                    Toast.makeText(ActivitiesActivity.this, getString(R.string.activity_error_permission), Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            default:
                Log.w(TAG, "Invalid request code received: " + requestCode);
        }
    }

    private void loadActivities() {
        LypeActivitiesApiService.getInstance(this).getApi().getActivities(location, filters, pageOffset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<LypeActivity>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getActivities onCompleted");
                        if(activities.size() > 0) {
                            updateWithActivity(activities.get(currentActivity));
                            onActivitiesLoaded();
                        } else {
                            setNoActivityLayoutVisibility(true);
                            updatePicture("https://ih0.redbubble.net/image.53589210.4056/flat,800x800,075,t.u1.jpg");
                            updateName(getString(R.string.activity_no_activity));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<LypeActivity> activities) {
                        ActivitiesActivity.this.activities.addAll(activities);
                        pageOffset++;
                    }
                });
    }

    private void loadComments(String activityId) {
        LypeActivitiesApiService.getInstance(this).getApi().getComments(activityId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Comment>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getComments onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<Comment> comments) {
                        updateComments(comments);
                    }
                });
    }

    private void onActivitiesLoaded() {
        /*if (PropertiesManager.isNewUser()) {
            initFeedbackDemo();
        } else {*/
            initSwipeListener();
        //}
    }

    private void saveChoice(boolean interested) {
        if(currentActivity < activities.size()) {
            if(mode != null && mode.equals(MODE_REVIEW)) {
                sendSuggestionFeedback(interested);
            } else {
                manageActivityInDatabase(interested);
                sendActivityFeedback(interested);
            }

            if(currentActivity == activities.size() - 1) {  //load the next page when the last activity is reached
                if(mode != null && mode.equals(MODE_REVIEW)) {
                    loadSuggestions();
                } else {
                    loadActivities();
                }
            }
        } else {
            if(interested) {
                blockScrolling(false);  //otherwise it would stay blocked when coming back
                Intent suggestionActivityIntent = new Intent(this, SuggestionActivity.class);
                startActivity(suggestionActivityIntent);
            } else {
                finish();
            }
        }
    }

    private void manageActivityInDatabase(boolean interested) {
        LypeActivity lypeActivity = activities.get(currentActivity);
        LypeActivity activity = LypeActivity.findFromServerId(this, lypeActivity.getServerId());
        if (interested && activity == null) {
            Log.i(TAG, "Storing activity in database");
            lypeActivity.save(this);
        } else if (!interested && activity != null) {
            Log.i(TAG, "Deleting activity from database");
            activity.delete(this);
        } else {
            Log.d(TAG, "No action performed on database");
        }
    }

    private void sendActivityFeedback(boolean interested) {
        try {
            LypeActivitiesApiService.getInstance(this).getApi()
                    .putActivityFeedback(activities.get(currentActivity).getServerId(), interested)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "putActivityFeedback onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(Boolean b) {
                            Log.d(TAG, "putActivityFeedback onNext: " + b);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nextActivity();
    }

    private void sendSuggestionFeedback(boolean approved) {
        try {
            LypeActivitiesApiService.getInstance(this).getApi()
                    .putSuggestionFeedback(activities.get(currentActivity).getServerId(), approved)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Boolean>() {
                        @Override
                        public void onCompleted() {
                            Log.d(TAG, "postSuggestionFeedback onCompleted");
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onNext(Boolean b) {
                            Log.d(TAG, "postSuggestionFeedback onNext: " + b);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nextActivity();
    }

    private void nextActivity() {
        blockScrolling(false);
        ++currentActivity;
        if(activities.size() > currentActivity) {
            updateWithActivity(activities.get(currentActivity));
        } else {
            Log.d(TAG, "Showing no_more_activity_layout");
            setNoMoreActivityLayoutVisibility(true);
            updatePicture("https://ih0.redbubble.net/image.53589210.4056/flat,800x800,075,t.u1.jpg");
            updateName(getString(R.string.activity_no_more));
        }
    }

    private void updateWithActivity(LypeActivity activity) {
        if(activity != null) {
            setNoMoreActivityLayoutVisibility(false);
            loadComments(activity.getServerId());   //Async load of comments

            Log.d(TAG, "Updating with activity " + activity.getName());
            updatePicture(activity.getPictureUrl());
            updateName(activity.getName());
            updateTags(activity.getFilters(this));
            updateDescription(activity.getDescription());
            updateWebsite(activity.getWebsite());
            updateAddress(activity.getAddress());
            updateMap(activity.getLocation(), activity.getAddress());
            updateComments(activity.getComments());
        } else {
            Log.d(TAG, "No more activity");
        }
    }

    private void setNoMoreActivityLayoutVisibility(boolean visible) {
        findViewById(R.id.activity_layout).setVisibility(visible ? View.GONE : View.VISIBLE);
        findViewById(R.id.no_activity_layout).setVisibility(View.GONE);
        findViewById(R.id.no_more_activity_layout).setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void setNoActivityLayoutVisibility(boolean visible) {
        findViewById(R.id.activity_layout).setVisibility(visible ? View.GONE : View.VISIBLE);
        findViewById(R.id.no_more_activity_layout).setVisibility(View.GONE);
        findViewById(R.id.no_activity_layout).setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void updatePicture(String imageUrl) {
        Picasso.with(this).load(imageUrl).into((ImageView) findViewById(R.id.backdrop));
    }

    private void updateName(String name) {
        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar)).setTitle(name);
    }

    private void updateTags(final List<FilterType> filterTypes) {
        FlowLayout tags = (FlowLayout) findViewById(R.id.tags);
        TagView.buildTagsLayout(this, tags, filterTypes);
    }

    private void updateDescription(String description) {
        ((TextView) findViewById(R.id.description)).setText(description);
    }

    private void updateWebsite(String website) {
        findViewById(R.id.web_site_card).setVisibility(website != null && !website.isEmpty() ? View.VISIBLE : View.GONE);
        ((TextView) findViewById(R.id.web_site)).setText(website);
    }

    private void updateAddress(String address) {
        ((TextView) findViewById(R.id.address)).setText(address);
    }

    private void updateMap(final Location location, final String address) {
        final ImageView map = (ImageView) findViewById(R.id.map);
        map.post(new Runnable() {
            @Override
            public void run() {
                try {
                    String mapRequest;
                    if (location != null)
                        mapRequest = String.format("http://maps.google.com/maps/api/staticmap?center=%s,%s&zoom=13&size=%sx%s&sensor=false&markers=color:red|%s,%s",
                                location.getLatitude(), location.getLongitude(), map.getWidth(), map.getHeight(), location.getLatitude(), location.getLongitude());
                    else
                        mapRequest = String.format("http://maps.google.com/maps/api/staticmap?zoom=13&size=%sx%s&sensor=false&markers=color:red|%s",
                                map.getWidth(), map.getHeight(), URLEncoder.encode(address, "UTF-8"));
                    Log.d(TAG, "MapRequest: " + mapRequest);
                    Picasso.with(ActivitiesActivity.this).load(mapRequest).into(map);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!preventClick) {
                    double latitude = location != null ? location.getLatitude() : 0;
                    double longitude = location != null ? location.getLongitude() : 0;
                    String place = address != null ? address : latitude + "," + longitude + "(" + activities.get(currentActivity).getName() + ")";
                    String uriString = String.format("geo:%s,%s?z=13&q=%s", latitude, longitude, place);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
                    startActivity(intent);
                }
            }
        });
    }

    private void updateComments(List<Comment> comments) {
        if(comments == null)
            comments = new ArrayList<>();
        if (commentAdapter == null)
            commentAdapter = new CommentAdapter(comments);
        else
            commentAdapter.replaceComments(comments);
        updateCommentsCount();
        RecyclerView commentsView = (RecyclerView) findViewById(R.id.comments);
        if(commentsView != null && commentsView.getAdapter() == null) {
            commentsView.setAdapter(commentAdapter);
            commentsView.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    private void updateCommentsCount() {
        int comments = commentAdapter.getItemCount();
        TextView commentsCount = (TextView) findViewById(R.id.comments_count);
        commentsCount.setText(String.format(getString(R.string.activity_comments) + " (%s)", comments));
    }

    private Spring createSpring(SimpleSpringListener simpleSpringListener) {
        // Create a system to run the physics loop for a set of springs.
        SpringSystem springSystem = SpringSystem.create();

        // Add a spring to the system.
        Spring spring = springSystem.createSpring();
        spring.setSpringConfig(new SpringConfig(150, 20));

        spring.addListener(simpleSpringListener);

        // Set the spring in motion; moving from 1 to 0
        spring.setCurrentValue(1);
        spring.setEndValue(0);

        return spring;
    }

    private void scrollToTop() {
        NestedScrollView nestedScrollView = (NestedScrollView) findViewById(R.id.scroll_view);
        nestedScrollView.setScrollY(0);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.setExpanded(true, false);
    }

    private void recenterViewWithSpringAnimation() {
        stopSpring = false;

        final View root = findViewById(R.id.root);
        final float initialX = root.getTranslationX();
        createSpring(new SimpleSpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                if(!stopSpring) {
                    float value = (float) spring.getCurrentValue();
                    float x = initialX * value;
                    root.setTranslationX(x);

                    boolean finished = value == 0;
                    preventClick = !finished;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(currentActivity > 0) {
            updateWithActivity(activities.get(--currentActivity));
        } else
            super.onBackPressed();
    }
}

package com.passionusnetworks.lype.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.login.LoginHelper;
import com.passionusnetworks.lype.login.LoginResultListener;
import com.passionusnetworks.lype.persistence.PropertiesManager;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONException;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2017-06-26.
 */

public class CreateAccountActivity extends FragmentActivity implements LoginResultListener {
    private static final String TAG = "CreateAccountActivity";

    private boolean success = false;
    private Handler handler;
    private LoginHelper loginHelper;

    @Override
    public void onCreate(Bundle args) {
        Log.d(TAG, "onCreate");
        super.onCreate(args);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_create_account);
        loginHelper = new LoginHelper(this, this);
        handler = new Handler();
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView email = (TextView) findViewById(R.id.email);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView createAccount = (TextView) findViewById(R.id.create_account);
        password.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    createAccount.performClick();
                }
                return false;
            }
        });
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = name.getText().toString();
                final String userEmail = email.getText().toString();
                final String userPassword = password.getText().toString();
                if(userName.isEmpty()) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CreateAccountActivity.this, getString(R.string.create_account_invalid_name), Toast.LENGTH_LONG).show();
                        }
                    });
                } else if(userEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CreateAccountActivity.this, getString(R.string.create_account_invalid_email), Toast.LENGTH_LONG).show();
                        }
                    });
                } else if(userPassword.isEmpty() || userPassword.length() < 6) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CreateAccountActivity.this, getString(R.string.create_account_invalid_password), Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    String GUID = PropertiesManager.getGUID();

                    try {
                        LypeActivitiesApiService.getInstance(CreateAccountActivity.this).getApi().postCreateAccount(userName, userEmail, userPassword, GUID)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Subscriber<String>() {
                                    @Override
                                    public void onCompleted() {
                                        Log.d(TAG, "postCreateAccount onCompleted");
                                        if (success) {
                                            PropertiesManager.setEmail(userEmail);
                                            PropertiesManager.setPassword(userPassword);
                                            PropertiesManager.saveProperties();
                                            loginHelper.loginWithEmail();
                                        }
                                    }

                                    @Override
                                    public void onError(final Throwable e) {
                                        success = false;
                                        e.printStackTrace();
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(CreateAccountActivity.this, R.string.create_account_create_fail, Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onNext(final String response) {
                                        Log.d(TAG, "onNext: " + response);
                                        if (response.equals("Email already used")) {
                                            success = false;
                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(CreateAccountActivity.this, R.string.create_account_email_used, Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        } else {
                                            success = true;
                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(CreateAccountActivity.this, getString(R.string.login_account_created), Toast.LENGTH_LONG).show();
                                                }
                                            });
                                        }
                                    }
                                });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onLoginSuccessful(final String username) {
        Log.d(TAG, "onLoginSuccessful");
        handler.post(new Runnable() {
            @Override
            public void run() {
                String text = username != null ? String.format(getString(R.string.login_success), username) : getString(R.string.login_success_without_name);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onLoginFailed(int stringId) {
        if(stringId > 0) {
            final String message = getString(stringId);
            Log.e(TAG, message);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(CreateAccountActivity.this, message, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Log.e(TAG, "onLoginFailed");
        }
    }
}

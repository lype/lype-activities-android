package com.passionusnetworks.lype.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.fragments.FilterFragment;
import com.passionusnetworks.lype.models.Filter;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raphael on 2016-04-18.
 */
public class FiltersActivity extends AppCompatActivity {
    private static final String TAG = "FiltersActivity";

    private List<FilterType> preselectedFilterTypes;
    private List<FilterType> filterTypes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        setTheme(R.style.NoActionBarTheme);
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            Log.d(TAG, "savedInstanceState is not null");
            preselectedFilterTypes = (List<FilterType>) savedInstanceState.getSerializable("filters");
        }
        setContentView(R.layout.activity_filters);
        initToolbar();
        loadFilters();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putSerializable("filters", (Serializable) filterTypes);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.filters_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                Intent filteredActivitiesIntent = new Intent(this, ActivitiesActivity.class);
                filteredActivitiesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                filteredActivitiesIntent.putExtra("filters", (Serializable) getSelectedFilters());
                startActivity(filteredActivitiesIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<FilterType> getSelectedFilters() {
        List<FilterType> selectedFilterTypes = new ArrayList<>();
        for(FilterType filterType : filterTypes) {
            List<Filter> selectedFilters = new ArrayList<>();
            for(Filter filter : filterType.getFilters(this)) {
                if(filter.isSelected()) {
                    selectedFilters.add(filter);
                }
            }
            if(!selectedFilters.isEmpty()) {
                selectedFilterTypes.add(new FilterType(filterType.getType(), selectedFilters));
                Log.d(TAG, "Filter type " + filterType.getType() + ": " + selectedFilters.size());
            }
        }
        return selectedFilterTypes;
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.filter_title));
    }

    private void loadFilters() {
        LypeActivitiesApiService.getInstance(this).getMockedApi().getFilters()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<FilterType>>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "getFilters onCompleted");
                        initViewPager();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<FilterType> loadedFilterTypes) {
                        filterTypes = loadedFilterTypes;
                    }
                });
    }

    private void getPreselectedFilters() {
        if(getIntent().hasExtra("filters")) {
            Log.d(TAG, "loading preselected filters");
            preselectedFilterTypes = (List<FilterType>) getIntent().getSerializableExtra("filters");
        }
        if(preselectedFilterTypes == null) {
            Log.d(TAG, "preselected filters are null");
            preselectedFilterTypes = new ArrayList<>();
        }
    }

    private void initViewPager() {
        FilterFragmentsAdapter filterFragmentsAdapter = new FilterFragmentsAdapter(getSupportFragmentManager());
        if(preselectedFilterTypes == null)
            getPreselectedFilters();
        for(FilterType filterType : filterTypes) {
            FilterFragment filterFragment = new FilterFragment();
            filterFragment.setFilterType(filterType);
            for(FilterType preselectedFilter : preselectedFilterTypes) {
                if(preselectedFilter.getType().equals(filterType.getType())) {
                    filterFragment.updateSelectedFilters(preselectedFilter);
                }
            }
            filterFragmentsAdapter.addFragment(filterFragment, filterType.getName());
        }
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(filterFragmentsAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        for(int i=0; i<filterTypes.size(); i++) {
            tabLayout.getTabAt(i).setIcon(getResources().getDrawable(filterTypes.get(i).getIconId()));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    static class FilterFragmentsAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public FilterFragmentsAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}

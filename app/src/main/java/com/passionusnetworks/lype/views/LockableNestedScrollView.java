package com.passionusnetworks.lype.views;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Raphael on 2016-12-27.
 */

public class LockableNestedScrollView extends NestedScrollView {
    private static final String TAG = "LockableNestedScroll...";

    private boolean preventScroll = false;

    public LockableNestedScrollView(Context context) {
        super(context);
    }

    public LockableNestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LockableNestedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setPreventScroll(boolean preventScroll) {
        this.preventScroll = preventScroll;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return !preventScroll && super.onTouchEvent(ev);
    }
}

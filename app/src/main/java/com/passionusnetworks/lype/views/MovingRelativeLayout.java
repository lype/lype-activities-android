package com.passionusnetworks.lype.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Raphael on 2016-04-13.
 */
public class MovingRelativeLayout extends RelativeLayout {
    private List<Observer> observers = new ArrayList<>();
    private Observable translationXObservable = new Observable();

    public MovingRelativeLayout(Context context) {
        super(context);
    }

    public MovingRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MovingRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void removeObservers() {
        observers.clear();
    }

    @Override
    public void setTranslationX(float translationX) {
        super.setTranslationX(translationX);
        notifyObservers();
    }

    private void notifyObservers() {
        for(Observer observer : observers)
            observer.update(translationXObservable, getTranslationX());
    }
}

package com.passionusnetworks.lype.views;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.models.Filter;
import com.passionusnetworks.lype.models.FilterType;
import com.passionusnetworks.lype.viewholders.LabelViewHolder;

import org.apmem.tools.layouts.FlowLayout;

import java.util.List;

/**
 * Created by Raphael on 2016-03-30.
 */
public class TagView {
    private static final String TAG = "TagView";

    public static View build(Activity activity, Filter filter, String filterType) {
        View labelView = activity.getLayoutInflater().inflate(R.layout.label_layout, null);
        LabelViewHolder labelViewHolder = new LabelViewHolder(labelView);
        Resources res = activity.getResources();
        if(filter.getIconId() == 0)
            filter.setIconId(FilterType.getIconIdFromType(filterType));
        Drawable coloredIcon = res.getDrawable(filter.getIconId());
        int cardColor;
        switch (filterType) {
            case FilterType.FILTER_TYPE_TIME:
                cardColor = res.getColor(R.color.complementoryGreen);
                break;
            case FilterType.FILTER_TYPE_PRICE:
                cardColor = res.getColor(R.color.complementoryPurple);
                break;
            case FilterType.FILTER_TYPE_SOCIAL:
                cardColor = res.getColor(R.color.complementoryYellow);
                break;
            case FilterType.FILTER_TYPE_DISTANCE:
                cardColor = res.getColor(R.color.complementoryTeal);
                break;
            case FilterType.FILTER_TYPE_PASSION:
                cardColor = res.getColor(R.color.colorPrimaryDark);
                break;
            default:
                cardColor = Color.WHITE;
        }
        labelViewHolder.card.setCardBackgroundColor(cardColor);
        if(coloredIcon != null)
            coloredIcon.setColorFilter(res.getColor(R.color.imageColor), PorterDuff.Mode.SRC_ATOP);
        labelViewHolder.icon.setImageDrawable(coloredIcon);
        labelViewHolder.text.setText(filter.getName());
        return labelView;
    }

    public static void buildTagsLayout(final Activity activity, final FlowLayout flowLayout, final List<FilterType> filterTypes) {
        flowLayout.removeAllViews();
        Log.d(TAG, filterTypes.size() + " filter types");
        for(FilterType filterType : filterTypes) {
            for(Filter filter : filterType.getFilters(activity)) {
                View view = TagView.build(activity, filter, filterType.getType());
                flowLayout.addView(view);
            }
        }
    }
}

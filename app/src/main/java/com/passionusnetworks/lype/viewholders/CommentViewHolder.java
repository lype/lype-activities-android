package com.passionusnetworks.lype.viewholders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.passionusnetworks.lype.R;

/**
 * Created by Raphael on 2016-03-29.
 */
public class CommentViewHolder extends RecyclerView.ViewHolder {
    public CardView card;
    public TextView username;
    public TextView title;
    public TextView content;

    public CommentViewHolder(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.comment_card);
        username = (TextView) itemView.findViewById(R.id.comment_username);
        title = (TextView) itemView.findViewById(R.id.comment_title);
        content = (TextView) itemView.findViewById(R.id.comment_content);
    }

}

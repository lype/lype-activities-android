package com.passionusnetworks.lype.viewholders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.passionusnetworks.lype.R;

/**
 * Created by Raphael on 2016-03-29.
 */
public class LabelViewHolder extends RecyclerView.ViewHolder {
    public CardView card;
    public ImageView icon;
    public TextView text;

    public LabelViewHolder(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.label_card);
        icon = (ImageView) itemView.findViewById(R.id.label_icon);
        text = (TextView) itemView.findViewById(R.id.label_text);
    }

}

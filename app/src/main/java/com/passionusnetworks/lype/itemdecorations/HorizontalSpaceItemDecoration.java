package com.passionusnetworks.lype.itemdecorations;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Raphael on 2017-01-09.
 */
public class HorizontalSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int mHorizontalSpaceHeight;

    public HorizontalSpaceItemDecoration(int mHorizontalSpaceHeight) {
        this.mHorizontalSpaceHeight = mHorizontalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getChildAdapterPosition(view) == 0)
            outRect.left = mHorizontalSpaceHeight;
        outRect.right = mHorizontalSpaceHeight;
    }
}

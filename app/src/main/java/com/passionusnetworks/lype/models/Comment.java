package com.passionusnetworks.lype.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 2016-03-17.
 */
public class Comment implements Serializable {
    private String id;
    private String title;
    private String content;
    private User user;

    public Comment(String id, String title, String content, User user) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public static List<Comment> fromJson(JSONArray json) throws JSONException {
        ArrayList<Comment> comments = new ArrayList<>();
        for(int i=0; i<json.length(); ++i)
            comments.add(fromJson(json.getJSONObject(i)));
        return comments;
    }

    public static Comment fromJson(JSONObject json) throws JSONException {
        String id = json.getString("id");
        String title = json.getString("title");
        String text = json.getString("text");
        User author = User.fromJson(json.getJSONObject("author"));
        return new Comment(id, title, text, author);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

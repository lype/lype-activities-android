package com.passionusnetworks.lype.models;

/**
 * Created by Raphael on 2016-03-17.
 */
public class Label {
    public static final String LABEL_TYPE_DISTANCE = "distance";
    public static final String LABEL_TYPE_PRICE = "price";
    public static final String LABEL_TYPE_SOCIAL = "social";
    public static final String LABEL_TYPE_TIME = "time";

    private String type;
    private String value;

    public Label(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package com.passionusnetworks.lype.models;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 2016-09-23.
 */

public class ActivitySuggestion {
    private String id;
    private String name;
    private String description;
    private String website;
    private Uri image;
    private String address;
    private LatLng location;
    private List<FilterType> tags;
    private String authorId;

    public ActivitySuggestion() {
        tags = new ArrayList<>();
    }

    public ActivitySuggestion(String name, String description, String website, Uri image, String address, LatLng location, List<FilterType> tags, String authorId) {
        this.name = name;
        this.description = description;
        this.website = website;
        this.image = image;
        this.address = address;
        this.location = location;
        this.tags = tags;
        this.authorId = authorId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }

    public File getImageFile(Context context) {
        return new File(getRealPathFromURI(context, image));
    }

    private String getRealPathFromURI(Context context, Uri contentURI) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public List<FilterType> getTags() {
        return tags;
    }

    public void setTags(List<FilterType> tags) {
        this.tags = tags;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }
}

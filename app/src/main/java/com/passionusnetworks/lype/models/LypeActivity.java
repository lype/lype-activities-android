package com.passionusnetworks.lype.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.util.Log;

import com.passionusnetworks.lype.persistence.DatabaseObject;
import com.passionusnetworks.lype.persistence.LypeDatabase;
import com.passionusnetworks.lype.services.http.LypeActivitiesApiService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Raphael on 2016-03-17.
 */
public class LypeActivity implements Serializable, DatabaseObject {
    private static final String TAG = "LypeActivity";

    public static final String TABLE_NAME = "LypeActivity";
    public static final String CREATE_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "server_id TEXT NOT NULL," +
            "name TEXT NOT NULL," +
            "picture_url TEXT NOT NULL," +
            "description TEXT NOT NULL," +
            "website TEXT," +
            "address TEXT," +
            "latitude REAL," +
            "longitude REAL" +
            ");";

    private long id;
    private String serverId;
    private String name;
    private String pictureUrl;

    private List<FilterType> filters;
    private String description;
    private String website;
    private String address;

    private Location location;
    private double latitude;
    private double longitude;
    private Date dateStart;
    private Date dateEnd;

    private List<Comment> comments;

    public LypeActivity() {

    }

    public LypeActivity(long id, String serverId, String name, String pictureUrl, String description, String website, String address, double latitude, double longitude) {
        this.id = id;
        this.serverId = serverId;
        this.name = name;
        this.pictureUrl = pictureUrl;
        this.description = description;
        this.website = website;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LypeActivity(String serverId, String name, String pictureUrl, List<FilterType> filters, String description, String website, String address, Location location, Date dateStart, Date dateEnd, List<Comment> comments) {
        this.serverId = serverId;
        this.name = name;
        this.pictureUrl = pictureUrl;
        this.filters = filters;
        this.description = description;
        this.website = website;
        this.address = address;
        this.location = location;
        if(location != null) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
        }
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.comments = comments;
    }

    private static LypeActivity activityFromJsonObject(JSONObject jsonActivity, Context context) throws JSONException {
        String id = jsonActivity.getString("id");
        String name = jsonActivity.getString("name");
        String description = jsonActivity.getString("description");
        JSONArray pictures = jsonActivity.getJSONArray("images");
        String pictureUrl = pictures.length() > 0 ? parsePictureURL(pictures.getString(0)) : null;
        String webSite = jsonActivity.getString("website");
        JSONObject locationObject = jsonActivity.getJSONObject("location");
        String address = locationObject.getString("address");
        Location location = null;
        if(!Double.isNaN(locationObject.optDouble("latitude")) && !Double.isNaN(locationObject.optDouble("longitude"))) {
            location = new Location("");
            location.setLatitude(locationObject.getDouble("latitude"));
            location.setLongitude(locationObject.getDouble("longitude"));
        }
        Date dateStart = new Date();
        Date dateEnd = new Date();
        ArrayList<Comment> comments = new ArrayList<>();
        JSONObject tags = jsonActivity.getJSONObject("tags");
        List<FilterType> filterTypes = FilterType.getFiltersFromJsonObject(tags, context);
        return new LypeActivity(id, name, pictureUrl, filterTypes, description, webSite, address, location, dateStart, dateEnd, comments);
    }

    private static String parsePictureURL(String pictureUrl) {
        if(pictureUrl != null && pictureUrl.startsWith("activities/")) {
            pictureUrl = LypeActivitiesApiService.apiHost + "/" + pictureUrl;
        }
        return pictureUrl;
    }

    public static List<LypeActivity> activitiesFromJsonArray(JSONArray jsonActivities, Context context) throws JSONException {
        List<LypeActivity> activities = new ArrayList<>();
        for(int i=0; i<jsonActivities.length(); i++)
            activities.add(activityFromJsonObject(jsonActivities.getJSONObject(i), context));
        return activities;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<FilterType> getFilters(Context context) {
        //lazy loading
        if (filters == null && getId() != 0) {
            Log.d(TAG, "FilterTypes were not loaded yet, loading them");
            filters = FilterType.findByActivityId(id, context);
            for(FilterType filterType : filters) {
                filterType.setActivity(this);
            }
        }
        return filters;
    }

    public void setFilters(List<FilterType> filters) {
        this.filters = filters;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        //lazy loading
        if(location == null && latitude != 0 && longitude != 0) {
            location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
        }
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public long save(Context context) {
        LypeDatabase db = LypeDatabase.getInstance(context);
        ContentValues contentValues = new ContentValues();
        contentValues.put("server_id", serverId);
        contentValues.put("name", name);
        contentValues.put("picture_url", pictureUrl);
        contentValues.put("description", description);
        contentValues.put("website", website);
        contentValues.put("address", address);
        contentValues.put("latitude", latitude);
        contentValues.put("longitude", longitude);
        id = db.getDb().insert("LypeActivity", null, contentValues);
        if(id > 0) {
            Log.i(TAG, "LypeActivity saved with id " + id);
            for (FilterType filterType : filters) {
                filterType.setActivityId(id);
                filterType.save(context);
            }
        } else {
            Log.e(TAG, "Failed to save LypeActivity");
        }
        return id;
    }

    public boolean delete(Context context) {
        int deleted = FilterType.deleteFromActivityId(context, id);
        Log.d(TAG, deleted + " filters deleted");
        LypeDatabase db = LypeDatabase.getInstance(context);
        return db.getDb().delete("LypeActivity", "id = ?", new String[]{String.valueOf(id)}) > 0;
    }

    public static LypeActivity findFromServerId(Context context, String serverId) {
        LypeDatabase db = LypeDatabase.getInstance(context);
        Cursor cursor = db.getDb().query("LypeActivity", null, "server_id = ?", new String[]{serverId}, null, null, null);
        LypeActivity activity = null;
        if(cursor.moveToNext())
            activity = getLypeActivityFromCursor(cursor);
        cursor.close();
        return activity;
    }

    public static List<LypeActivity> findAll(Context context) {
        ArrayList<LypeActivity> activities = new ArrayList<>();
        LypeDatabase db = LypeDatabase.getInstance(context);
        Cursor cursor = db.getDb().query("LypeActivity", null, null, null, null, null, "id DESC");
        while(cursor.moveToNext()) {
            activities.add(getLypeActivityFromCursor(cursor));
        }
        cursor.close();
        return activities;
    }

    private static LypeActivity getLypeActivityFromCursor(Cursor cursor) {
        try {
            long id = cursor.getLong(0);
            String serverId = cursor.getString(1);
            String name = cursor.getString(2);
            String pictureUrl = cursor.getString(3);
            String description = cursor.getString(4);
            String website = cursor.getString(5);
            String address = cursor.getString(6);
            double latitude = cursor.getDouble(7);
            double longitude = cursor.getDouble(8);
            return new LypeActivity(id, serverId, name, pictureUrl, description, website, address, latitude, longitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
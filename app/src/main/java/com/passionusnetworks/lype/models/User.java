package com.passionusnetworks.lype.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Raphael on 2016-03-17.
 */
public class User implements Serializable {
    private String id;
    private String username;
    private String pictureUrl;

    public User(String id, String username, String pictureUrl) {
        this.id = id;
        this.username = username;
        this.pictureUrl = pictureUrl;
    }

    public static User fromJson(JSONObject json) throws JSONException {
        String id = json.getString("id");
        String name = json.getString("name");
        String profilePicture = json.getString("profilepicture");
        return new User(id, name, profilePicture);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}

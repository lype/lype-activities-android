package com.passionusnetworks.lype.models;

import android.content.Context;
import android.graphics.drawable.Drawable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Raphael on 2016-02-27.
 */
public class Category {
    public final static String CATEGORY_ANYTHING = "0";
    public final static String CATEGORY_MORE = "-1";

    private String id;
    private String name;
    private String imageUrl;
    private Drawable image;
    private List<FilterType> filters;

    public Category(String id, String name, String imageUrl, List<FilterType> filters) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.filters = filters;
    }

    private static Category categoryFromJsonObject(JSONObject jsonCategory, Context context) throws JSONException {
        String id = jsonCategory.getString("id");
        String name = jsonCategory.getString("translated_name");
        if(name == null || name.equals("null")) {
            name = jsonCategory.getString("name");
        }
        String imageUrl = jsonCategory.getString("image");
        JSONObject tags = jsonCategory.getJSONObject("tags");
        List<FilterType> filterTypes = FilterType.getFiltersFromJsonObject(tags, context);
        return new Category(id, name, imageUrl, filterTypes);
    }

    public static List<Category> categoriesFromJsonArray(JSONArray jsonCategories, Context context) throws JSONException {
        List<Category> categories = new ArrayList<>();
        for(int i=0; i<jsonCategories.length(); i++)
            categories.add(categoryFromJsonObject(jsonCategories.getJSONObject(i), context));
        return categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public List<FilterType> getFilters() {
        return filters;
    }

    public void setFilters(List<FilterType> filters) {
        this.filters = filters;
    }
}

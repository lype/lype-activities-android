package com.passionusnetworks.lype.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.passionusnetworks.lype.persistence.DatabaseObject;
import com.passionusnetworks.lype.persistence.LypeDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class Filter implements Serializable, DatabaseObject {
    private static final String TAG = "Filter";

    public static final String TABLE_NAME = "Filter";
    public static final String CREATE_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "value TEXT NOT NULL," +
            "icon_id INTEGER," +
            "name TEXT NOT NULL," +
            "filter_type_id INTEGER," +
            "FOREIGN KEY(filter_type_id) REFERENCES FilterType(id)" +
            ");";

    public long id;
    private String value;
    private int iconId;
    private String name;
    private long filterTypeId;
    private boolean isSelected;
    private FilterType filterType;

    public Filter() {

    }

    public Filter(long id, String value, int iconId, String name, long filterTypeId) {
        this.id = id;
        this.value = value;
        this.iconId = iconId;
        this.name = name;
        this.filterTypeId = filterTypeId;
        this.isSelected = false;
    }

    public Filter(String value, int iconId, String name) {
        this.value = value;
        this.iconId = iconId;
        this.name = name;
        this.isSelected = false;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public FilterType getFilterType(Context context) {
        if(filterType == null && filterTypeId != 0) {
            Log.d(TAG, "FilterType was not loaded yet, loading it");
            filterType = FilterType.findById(filterTypeId, context);
        }
        return filterType;
    }

    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
        this.filterTypeId = filterType.getId();
    }

    public long getFilterTypeId() {
        return filterTypeId;
    }

    public void setFilterTypeId(long filterTypeId) {
        this.filterTypeId = filterTypeId;
    }

    @Override
    public long save(Context context) {
        LypeDatabase db = LypeDatabase.getInstance(context);
        ContentValues contentValues = new ContentValues();
        contentValues.put("value", value);
        contentValues.put("icon_id", iconId);
        contentValues.put("name", name);
        contentValues.put("filter_type_id", filterTypeId);
        id = db.getDb().insert("Filter", null, contentValues);
        if(id > 0) {
            Log.i(TAG, this.toString() + " saved");
        } else {
            Log.e(TAG, "Failed to save filter");
        }
        return id;
    }

    @Override
    public boolean delete(Context context) {
        LypeDatabase db = LypeDatabase.getInstance(context);
        return db.getDb().delete("Filter", "id = ?", new String[]{String.valueOf(id)}) > 0;
    }

    public static int deleteFromFilterTypeId(Context context, long filterTypeId) {
        LypeDatabase db = LypeDatabase.getInstance(context);
        return db.getDb().delete("Filter", "filter_type_id = ?", new String[]{String.valueOf(filterTypeId)});
    }

    public static List<Filter> findByFilterTypeId(long filterTypeId, Context context) {
        List<Filter> filters = new ArrayList<>();
        LypeDatabase db = LypeDatabase.getInstance(context);
        Cursor cursor = db.getDb().query("Filter", null, "filter_type_id = ?", new String[]{String.valueOf(filterTypeId)}, null, null, "id DESC");
        try {
            while(cursor.moveToNext()) {
                long id = cursor.getLong(0);
                String value = cursor.getString(1);
                int iconId = cursor.getInt(2);
                String name = cursor.getString(3);
                filters.add(new Filter(id, value, iconId, name, filterTypeId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cursor.close();
        return filters;
    }

    public String toString() {
        return "<Filter id:" + id + ", value:" + value + " selected:" + isSelected + " filter_type_id:" + filterTypeId + ">";
    }
}

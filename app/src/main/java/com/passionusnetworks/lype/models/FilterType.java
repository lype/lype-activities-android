package com.passionusnetworks.lype.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.persistence.DatabaseObject;
import com.passionusnetworks.lype.persistence.LypeDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class FilterType implements Serializable, DatabaseObject {
    private final static String TAG = "FilterType";

    public final static String FILTER_TYPE_TIME = "time";
    public final static String FILTER_TYPE_PRICE = "price";
    public final static String FILTER_TYPE_SOCIAL = "social";
    public final static String FILTER_TYPE_DISTANCE = "distance";
    public final static String FILTER_TYPE_PASSION = "passion";

    public static final String TABLE_NAME = "FilterType";
    public static final String CREATE_QUERY = "CREATE TABLE " + TABLE_NAME + " (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "type TEXT NOT NULL," +
            "activity_id INTEGER," +
            "FOREIGN KEY(activity_id) REFERENCES LypeActivity(id)" +
            ");";

    private long id;
    private String type;
    private int iconId;
    private String name;
    private long activityId;

    private List<Filter> filters;
    private LypeActivity activity;
    //private static List<FilterType> allFilters;

    public FilterType() {

    }

    public FilterType(long id, String type, int iconId, String name, long activityId) {
        this.id = id;
        this.type = type;
        this.iconId = iconId;
        this.name = name;
        this.activityId = activityId;
    }

    public FilterType(String type, int iconId, String name, List<Filter> filters) {
        this.type = type;
        this.iconId = iconId;
        this.name = name;
        this.filters = filters;
    }

    public FilterType(String type, List<Filter> filters) {
        this.type = type;
        this.iconId = getIconIdFromType(type);
        this.name = null;
        this.filters = filters;
    }

    public FilterType(Context context, long id, String type, long activityId) {
        this.id = id;
        this.type = type;
        this.iconId = getIconIdFromType(type);
        this.name = getNameFromType(context, type);
        this.activityId = activityId;
    }

    public FilterType(Context context, String type, List<Filter> filters) {
        this.type = type;
        this.iconId = getIconIdFromType(type);
        this.name = getNameFromType(context, type);
        this.filters = filters;
    }

    public static int getIconIdFromType(String type) {
        switch (type) {
            case FILTER_TYPE_TIME:
                return R.drawable.ic_schedule_white_24dp;
            case FILTER_TYPE_PRICE:
                return R.drawable.ic_monetization_on_white_24dp;
            case FILTER_TYPE_SOCIAL:
                return R.drawable.ic_people_white_24dp;
            case FILTER_TYPE_DISTANCE:
                return R.drawable.ic_place_white_24dp;
            case FILTER_TYPE_PASSION:
                return R.drawable.ic_favorite_border_white_24dp;
        }
        return 0;
    }

    private static String getNameFromType(Context context, String type) {
        switch (type) {
            case FILTER_TYPE_TIME:
                return context.getResources().getString(R.string.filter_type_time);
            case FILTER_TYPE_PRICE:
                return context.getResources().getString(R.string.filter_type_price);
            case FILTER_TYPE_SOCIAL:
                return context.getResources().getString(R.string.filter_type_social);
            case FILTER_TYPE_DISTANCE:
                return context.getResources().getString(R.string.filter_type_distance);
            case FILTER_TYPE_PASSION:
                return context.getResources().getString(R.string.filter_type_passion);
        }
        return null;
    }

    public static JSONObject jsonObjectFromFilterTypes(List<FilterType> filterTypes) throws JSONException {
        JSONObject json = new JSONObject();
        for(FilterType filterType : filterTypes) {
            json.put(filterType.type, filterType.toJson());
        }
        return json;
    }

    private JSONArray toJson() throws JSONException {
        JSONArray json = new JSONArray();
        for(Filter filter : filters) {
            if(filter.isSelected())
                json.put(filter.getValue());
        }
        return json;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Filter> getFilters(Context context) {
        //lazy loading
        if (filters == null && getId() != 0) {
            Log.d(TAG, "Filters were not loaded yet, loading them");
            filters = Filter.findByFilterTypeId(id, context);
            for(Filter filter : filters) {
                filter.setFilterType(this);
            }
        }
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public LypeActivity getActivity() {
        return activity;
    }

    public void setActivity(LypeActivity activity) {
        this.activity = activity;
        this.activityId = activity.getId();
    }

    public long getActivityId(){
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    @Override
    public long save(Context context) {
        LypeDatabase db = LypeDatabase.getInstance(context);
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", type);
        contentValues.put("activity_id", activityId);
        id = db.getDb().insert("FilterType", null, contentValues);
        if(id > 0) {
            Log.i(TAG, this.toString() + " saved");
            for (Filter filter : filters) {
                filter.setFilterTypeId(id);
                filter.save(context);
            }
        } else {
            Log.e(TAG, "Failed to save FilterType");
        }
        return id;
    }

    @Override
    public boolean delete(Context context) {
        int deleted = Filter.deleteFromFilterTypeId(context, id);
        Log.d(TAG, deleted + " filters deleted");
        LypeDatabase db = LypeDatabase.getInstance(context);
        return db.getDb().delete("FilterType", "id = ?", new String[]{String.valueOf(id)}) > 0;
    }

    public static int deleteFromActivityId(Context context, long activityId) {
        List<FilterType> filterTypes = findByActivityId(activityId, context);
        int deleted = 0;
        for(FilterType filterType : filterTypes) {
            if(!filterType.delete(context))
                Log.e(TAG, "Failed to delete filterType of id " + filterType.id);
            else
                deleted++;
        }
        Log.d(TAG, deleted + " filters deleted");
        LypeDatabase db = LypeDatabase.getInstance(context);
        return db.getDb().delete("FilterType", "activity_id = ?", new String[]{String.valueOf(activityId)});
    }

    public static FilterType findById(long id, Context context) {
        FilterType filterType = null;
        LypeDatabase db = LypeDatabase.getInstance(context);
        Cursor cursor = db.getDb().query("FilterType", null, "id = ?", new String[]{String.valueOf(id)}, null, null, null);
        try {
            if(cursor.moveToNext()) {
                String type = cursor.getString(1);
                long activityId = cursor.getLong(2);
                filterType = new FilterType(context, id, type, activityId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cursor.close();
        return filterType;
    }

    public static List<FilterType> findByActivityId(long activityId, Context context) {
        List<FilterType> filters = new ArrayList<>();
        LypeDatabase db = LypeDatabase.getInstance(context);
        Cursor cursor = db.getDb().query("FilterType", null, "activity_id = ?", new String[]{String.valueOf(activityId)}, null, null, "id DESC");
        try {
            while(cursor.moveToNext()) {
                long id = cursor.getLong(0);
                String type = cursor.getString(1);
                filters.add(new FilterType(context, id, type, activityId));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cursor.close();
        Log.d(TAG, filters.size() + " filters loaded from activity id " + activityId);
        return filters;
    }

    public static List<FilterType> getFiltersFromJsonObject(JSONObject tags, Context context) throws JSONException {
        List<FilterType> allFilterTypes = getAllFilters(context);
        ArrayList<FilterType> filterTypes = new ArrayList<>();
        Iterator<String> iterator = tags.keys();
        while(iterator.hasNext()) {
            String tag = iterator.next();
            List<Filter> allFilters = null;
            for(int i=0; i<allFilterTypes.size(); ++i) {
                if(allFilterTypes.get(i).getType().equals(tag)) {
                    allFilters = allFilterTypes.get(i).getFilters(context);
                    break;
                }
            }
            JSONArray tagValues = tags.getJSONArray(tag);
            ArrayList<Filter> filters = new ArrayList<>();
            for(int i=0; i<tagValues.length(); i++) {
                if (allFilters != null) {
                    for (int j = 0; j < allFilters.size(); ++j) {
                        if(allFilters.get(j).getValue().equals(tagValues.getString(i))) {
                            filters.add(allFilters.get(j));
                            break;
                        }
                    }
                }
            }
            filterTypes.add(new FilterType(tag, filters));
        }
        return filterTypes;
    }

    public static List<FilterType> getAllFilters(Context context) {
        /*if(allFilters != null) {
            for(FilterType filterType : allFilters) {
                for(Filter filter : filterType.getFilters(context)) {
                    filter.setSelected(false);
                }
            }
            return allFilters;
        }*/
        List<FilterType> allFilters = new ArrayList<>();
        List<Filter> timeFilters = new ArrayList<>();
        timeFilters.add(new Filter("morning", R.drawable.ic_schedule_white_24dp, context.getString(R.string.filter_time_morning)));
        timeFilters.add(new Filter("afternoon", R.drawable.ic_schedule_white_24dp, context.getString(R.string.filter_time_afternoon)));
        timeFilters.add(new Filter("evening", R.drawable.ic_schedule_white_24dp, context.getString(R.string.filter_time_evening)));
        allFilters.add(new FilterType(FilterType.FILTER_TYPE_TIME, R.drawable.ic_schedule_white_24dp, context.getResources().getString(R.string.filter_type_time), timeFilters));
        List<Filter> priceFilters = new ArrayList<>();
        priceFilters.add(new Filter("free", R.drawable.ic_monetization_on_white_24dp, context.getString(R.string.filter_price_free)));
        priceFilters.add(new Filter("cheap", R.drawable.ic_monetization_on_white_24dp, context.getString(R.string.filter_time_cheap)));
        priceFilters.add(new Filter("average", R.drawable.ic_monetization_on_white_24dp, context.getString(R.string.filter_time_average)));
        priceFilters.add(new Filter("expensive", R.drawable.ic_monetization_on_white_24dp, context.getString(R.string.filter_time_expensive)));
        allFilters.add(new FilterType(FilterType.FILTER_TYPE_PRICE, R.drawable.ic_monetization_on_white_24dp, context.getResources().getString(R.string.filter_type_price), priceFilters));
        List<Filter> socialFilters = new ArrayList<>();
        socialFilters.add(new Filter("alone", R.drawable.ic_people_white_24dp, context.getString(R.string.filter_social_alone)));
        socialFilters.add(new Filter("couple", R.drawable.ic_people_white_24dp, context.getString(R.string.filter_social_couple)));
        socialFilters.add(new Filter("friends", R.drawable.ic_people_white_24dp, context.getString(R.string.filter_social_friends)));
        socialFilters.add(new Filter("family", R.drawable.ic_people_white_24dp, context.getString(R.string.filter_social_family)));
        allFilters.add(new FilterType(FilterType.FILTER_TYPE_SOCIAL, R.drawable.ic_people_white_24dp, context.getResources().getString(R.string.filter_type_social), socialFilters));
        List<Filter> distanceFilters = new ArrayList<>();
        distanceFilters.add(new Filter("0-5", R.drawable.ic_place_white_24dp, context.getString(R.string.filter_distance_very_close)));
        distanceFilters.add(new Filter("5-25", R.drawable.ic_place_white_24dp, context.getString(R.string.filter_distance_near)));
        distanceFilters.add(new Filter("25-250", R.drawable.ic_place_white_24dp, context.getString(R.string.filter_distance_far)));
        allFilters.add(new FilterType(FilterType.FILTER_TYPE_DISTANCE, R.drawable.ic_place_white_24dp, context.getResources().getString(R.string.filter_type_distance), distanceFilters));
        List<Filter> passionFilters = new ArrayList<>();
        passionFilters.add(new Filter("sport", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_sport)));
        passionFilters.add(new Filter("fashion", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_fashion)));
        passionFilters.add(new Filter("tech", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_tech)));
        passionFilters.add(new Filter("music", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_music)));
        passionFilters.add(new Filter("food", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_food)));
        passionFilters.add(new Filter("relaxation", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_relaxation)));
        passionFilters.add(new Filter("educative", R.drawable.ic_favorite_border_white_24dp, context.getString(R.string.filter_passion_educative)));
        allFilters.add(new FilterType(FilterType.FILTER_TYPE_PASSION, R.drawable.ic_favorite_border_white_24dp, context.getResources().getString(R.string.filter_type_passion), passionFilters));
        return allFilters;
    }

    public String toString() {
        return "<FilterType id:" + id + ", type:" + type + ", activity_id:" + activityId + ">";
    }
}

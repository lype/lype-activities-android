package com.passionusnetworks.lype.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.models.Comment;
import com.passionusnetworks.lype.viewholders.CommentViewHolder;

import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentViewHolder> {
    private List<Comment> comments;

    public CommentAdapter(List<Comment> comments) {
        this.comments = comments;
    }

    public void replaceComments(List<Comment> comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    public void addComment(Comment comment) {
        comments.add(0, comment);
        notifyItemRangeInserted(0, 1);
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_layout, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CommentViewHolder holder, int position) {
        final Comment comment = comments.get(position);
        holder.title.setText(comment.getTitle());
        holder.username.setText(comment.getUser().getUsername());
        holder.content.setText(comment.getContent());
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }
}

package com.passionusnetworks.lype.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.models.Filter;

import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterViewHolder> {
    private Context context;
    private List<Filter> filters;

    public FilterAdapter(Context context, List<Filter> filters) {
        this.context = context;
        this.filters = filters;
    }

    @Override
    public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_item, parent, false);
        return new FilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FilterViewHolder holder, int position) {
        final Filter filter = filters.get(position);
        holder.setStyle(context, filter);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter.setSelected(!filter.isSelected());
                holder.setStyle(context, filter);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    public static class FilterViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView icon;
        public TextView name;

        public FilterViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView;
            icon = (ImageView) itemView.findViewById(R.id.icon);
            name = (TextView) itemView.findViewById(R.id.name);
        }

        public void setStyle(final Context context, final Filter filter) {
            icon.setImageDrawable(context.getResources().getDrawable(filter.getIconId()));
            name.setText(filter.getName());
            if(filter.isSelected()) {
                cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                icon.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                name.setTextColor(Color.WHITE);
            } else {
                cardView.setCardBackgroundColor(Color.WHITE);
                icon.setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                name.setTextColor(context.getResources().getColor(R.color.colorAccent));
            }
        }
    }
}

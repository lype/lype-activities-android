package com.passionusnetworks.lype.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.activities.ActivitiesActivity;
import com.passionusnetworks.lype.listeners.FavoritesListListener;
import com.passionusnetworks.lype.models.LypeActivity;
import com.passionusnetworks.lype.views.TagView;
import com.squareup.picasso.Picasso;

import org.apmem.tools.layouts.FlowLayout;

import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {
    private static final String TAG = "FavoriteAdapter";

    private Activity activity;
    private List<LypeActivity> favorites;
    private FavoritesListListener listener;
    private LypeActivity deleted;
    private int deletedIndex;

    public FavoriteAdapter(Activity activity, List<LypeActivity> favorites, FavoritesListListener listener) {
        this.activity = activity;
        this.favorites = favorites;
        this.listener = listener;
    }

    @Override
    public FavoriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_item, parent, false);
        return new FavoriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FavoriteViewHolder holder, int position) {
        LypeActivity favorite = favorites.get(position);
        final String serverId = favorite.getServerId();
        holder.setActivity(activity, favorite);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ActivitiesActivity.class);
                intent.putExtra(ActivitiesActivity.EXTRA_MODE, ActivitiesActivity.MODE_FAVORITE);
                intent.putExtra(ActivitiesActivity.EXTRA_FAVORITE, serverId);
                activity.startActivity(intent);
            }
        });
        if(position == favorites.size() - 1) {
            listener.onLastItemReached();
        }
    }

    @Override
    public int getItemCount() {
        return favorites.size();
    }

    public LypeActivity removeItem(int index) {
        deleted = favorites.remove(index);
        deletedIndex = index;
        notifyItemRemoved(index);
        return deleted;
    }

    public void undoRemove() {
        favorites.add(deletedIndex, deleted);
        notifyItemInserted(deletedIndex);
    }

    public void addFavorites(List<LypeActivity> favorites) {
        notifyItemRangeInserted(this.favorites.size() - favorites.size(), favorites.size());
    }

    public static class FavoriteViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView imageView;
        public TextView name;
        public FlowLayout filters;

        public FavoriteViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView;
            imageView = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            filters = (FlowLayout) itemView.findViewById(R.id.filters);
        }

        public void setActivity(final Activity activity, final LypeActivity lypeActivity) {
            Picasso.with(activity).load(lypeActivity.getPictureUrl()).into(imageView);
            name.setText(lypeActivity.getName());
            TagView.buildTagsLayout(activity, filters, lypeActivity.getFilters(activity));
        }
    }
}

package com.passionusnetworks.lype.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.passionusnetworks.lype.R;
import com.passionusnetworks.lype.activities.ActivitiesActivity;
import com.passionusnetworks.lype.models.Category;
import com.passionusnetworks.lype.views.TagView;
import com.squareup.picasso.Picasso;

import org.apmem.tools.layouts.FlowLayout;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Raphael on 2016-04-18.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private Activity activity;
    private List<Category> categories;

    public CategoryAdapter(Activity activity, List<Category> categories) {
        this.activity = activity;
        this.categories = categories;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, int position) {
        final Category category = categories.get(position);
        holder.setCategory(activity, category);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ActivitiesActivity.class);
                intent.putExtra("filters", (Serializable) category.getFilters());
                activity.startActivity(intent);
            }
        });
        float cardWidth = ActivitiesActivity.getScreenWidth(activity) * 0.75f;
        RecyclerView.LayoutParams cardViewLayoutParams = new RecyclerView.LayoutParams((int) cardWidth, ViewGroup.LayoutParams.MATCH_PARENT);
        cardViewLayoutParams.setMargins(0, 30, 0, 30);
        holder.cardView.setLayoutParams(cardViewLayoutParams);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView imageView;
        public TextView name;
        public FlowLayout filters;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView;
            imageView = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            filters = (FlowLayout) itemView.findViewById(R.id.filters);
        }

        public void setCategory(final Activity activity, final Category category) {
            Picasso.with(activity).load(category.getImageUrl()).into(imageView);
            name.setText(category.getName());
            TagView.buildTagsLayout(activity, filters, category.getFilters());
        }
    }
}
